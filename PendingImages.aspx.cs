﻿using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class PendingImages : Page, IRequiresSessionState
{
    //protected HtmlForm form1;
    //protected GridView gvData;

    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells.Count == 6))
        {
            e.Row.Cells[5].Visible = false;
            string text = "";
            text = e.Row.Cells[5].Text;
            e.Row.Cells[3].Text = "<a href='http://captchatypers.com/CAPTCHAImages/" + text + "' target='_blank'>" + e.Row.Cells[3].Text + "</a>";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SqlCommand command = new SqlCommand {
                CommandText = "GetReportForAll_ahic",
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add("@ReportType", SqlDbType.TinyInt).Value = 1;
            Raheel raheel = new Raheel {
                Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]
            };
            this.gvData.DataSource = raheel.getPendingImages(0x5aa);
            this.gvData.DataBind();
            raheel.Dispose();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile) this.Context.Profile;
    //    }
    //}
}


