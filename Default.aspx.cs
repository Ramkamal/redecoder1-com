﻿using System;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class _Default : Page, IRequiresSessionState 
{
    //protected HtmlForm form1;

    protected void Page_Load(object sender, EventArgs e)
    {
        base.Response.Redirect("~/Forms/Home.aspx");
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

