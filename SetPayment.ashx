﻿<%@ WebHandler Language="C#" Class="SetPayment" %>

using System;
using System.Configuration; 
using Microsoft.VisualBasic; 
using System.Data;
using System.Data.SqlClient;
using MK;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class SetPayment : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        context.Response.ContentType = "text/plain";
        //context.Response.Write(Convert.ToString(context.Request.QueryString["Username"]));
        //context.Response.Write(Convert.ToString(context.Request.QueryString["Amount"]));

        if (context.Request.QueryString["Username"] == null)
        {
            //context.Response.Write("username is nothing");
        }
        else
        {
           // context.Response.Write("username is coming");
        }


        if (context.Request.QueryString["Amount"] == null)
        {
            //context.Response.Write("Amount is nothing");
        }
        else
        {
            //context.Response.Write("Amount is coming");
        }

        string strOrderNo = "";

        if (context.Request.QueryString["ORDERNO"] == null)
        {
            strOrderNo = "";
        }
        else
        {
            strOrderNo = context.Request.QueryString["ORDERNO"];
        }

        try
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "getClientITDetails_hnaz";
            sqlCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = context.Request.QueryString["Username"];

            RAHEEL.Raheel ws = new RAHEEL.Raheel();
            ws.Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]; // "http://www.imagetyperz.com/RAHEEL.ASMX";
            string strUsername = Convert.ToString(CommonFunctions.ReturnDatasource(sqlCmd).Tables[0].Rows[0]["ITUsername"]);
            ws.SetPayment(strUsername, Convert.ToSingle(context.Request.QueryString["Amount"]), 1450, strOrderNo, context.Request.QueryString["Username"]);
            ws.Dispose();

            HttpContext.Current.Response.Write("SUCCESS");
        }
        catch (Exception ex)
        {
            if (context.Request.QueryString["Username"] == null)
            {
                throw new Exception("username is nothing");
            }
            else
            {
                HttpContext.Current.Response.Write(ex.Message);
            }


            if (context.Request.QueryString["Amount"] == null)
            {
                throw new Exception("Amount is nothing");
            }
            else
            {
                HttpContext.Current.Response.Write(ex.Message);
            } 
        }

    }

    public bool IsReusable
    {
        get {
            return false;
        }
    }

}