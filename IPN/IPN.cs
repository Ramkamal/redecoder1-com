﻿namespace IPN
{
    using Microsoft.VisualBasic;
    using Microsoft.VisualBasic.CompilerServices;
    using System;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;

    public class IPN
    {
        public object ArrayExpand(object Vector)
        {
            object obj2;
            object obj3;
            object obj7;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj3, 0, NewLateBinding.LateGet(Vector, null, "Count", new object[0], null, null, null), 1, ref obj7, ref obj3))
            {
                do
                {
                    object obj4;
                    int right = Strings.Len(RuntimeHelpers.GetObjectValue(this.StripSlashes(Conversions.ToString(NewLateBinding.LateIndexGet(Vector, new object[] { RuntimeHelpers.GetObjectValue(obj3) }, null)))));
                    obj4 = Operators.ConcatenateObject(Operators.ConcatenateObject(obj4, right), this.StripSlashes(Conversions.ToString(NewLateBinding.LateIndexGet(Vector, new object[] { RuntimeHelpers.GetObjectValue(obj3) }, null))));
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj3, obj7, ref obj3));
            }
            return obj2;
        }

        public object HexToDec(object hexVal)
        {
            return Conversions.ToLong(Operators.ConcatenateObject("&H", hexVal));
        }

        public object hmac(object key, object data)
        {
            MD5 md;
            int no = 0x40;
            if (Strings.Len(RuntimeHelpers.GetObjectValue(key)) > no)
            {
                key = RuntimeHelpers.GetObjectValue(this.pack(md.Hash[Conversions.ToInteger(key)]));
            }
            char str = '\0';
            char ch2 = '6';
            char ch3 = '\\';
            key = RuntimeHelpers.GetObjectValue(this.str_pad(RuntimeHelpers.GetObjectValue(key), no, str));
            object objectValue = RuntimeHelpers.GetObjectValue(this.str_pad("", no, ch2));
            object obj6 = RuntimeHelpers.GetObjectValue(this.str_pad("", no, ch3));
            object left = RuntimeHelpers.GetObjectValue(this.myXor(RuntimeHelpers.GetObjectValue(key), RuntimeHelpers.GetObjectValue(obj6)));
            object obj4 = RuntimeHelpers.GetObjectValue(this.myXor(RuntimeHelpers.GetObjectValue(key), RuntimeHelpers.GetObjectValue(objectValue)));
            return md.Hash[Conversions.ToInteger(Operators.ConcatenateObject(left, this.pack(md.Hash[Conversions.ToInteger(Operators.ConcatenateObject(obj4, data))])))];
        }

        public object myAnd(object str1, object str2)
        {
            object obj2;
            object obj5;
            object obj8;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj2, 1, Strings.Len(RuntimeHelpers.GetObjectValue(str1)), 1, ref obj8, ref obj2))
            {
                do
                {
                    obj5 = Operators.ConcatenateObject(obj5, Strings.Chr(Strings.Asc(Strings.Mid(Conversions.ToString(str1), Conversions.ToInteger(obj2), 1)) & Strings.Asc(Strings.Mid(Conversions.ToString(str2), Conversions.ToInteger(obj2), 1))));
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj2, obj8, ref obj2));
            }
            return RuntimeHelpers.GetObjectValue(obj5);
        }

        public object myXor(object str1, object str2)
        {
            object obj2;
            object obj5;
            object obj8;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj2, 1, Strings.Len(RuntimeHelpers.GetObjectValue(str1)), 1, ref obj8, ref obj2))
            {
                do
                {
                    obj5 = Operators.ConcatenateObject(obj5, Strings.Chr(Strings.Asc(Strings.Mid(Conversions.ToString(str1), Conversions.ToInteger(obj2), 1)) ^ Strings.Asc(Strings.Mid(Conversions.ToString(str2), Conversions.ToInteger(obj2), 1))));
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj2, obj8, ref obj2));
            }
            return RuntimeHelpers.GetObjectValue(obj5);
        }

        public object pack(object S)
        {
            object obj2;
            object obj3;
            object obj7;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj2, 1, Strings.Len(RuntimeHelpers.GetObjectValue(S)), 1, ref obj7, ref obj2))
            {
                do
                {
                    obj3 = Operators.ConcatenateObject(obj3, Strings.Chr(Conversions.ToInteger(this.HexToDec(Strings.Mid(Conversions.ToString(S), Conversions.ToInteger(obj2), 2)))));
                    obj2 = Operators.AddObject(obj2, 1);
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj2, obj7, ref obj2));
            }
            return RuntimeHelpers.GetObjectValue(obj3);
        }

        public object str_pad(object originalString, object no, object str)
        {
            object obj3;
            object obj7;
            int right = Strings.Len(RuntimeHelpers.GetObjectValue(originalString));
            object left = Operators.SubtractObject(no, right);
            if (Operators.ConditionalCompareObjectGreater(left, 0, false) && ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj3, 1, left, 1, ref obj7, ref obj3))
            {
                do
                {
                    originalString = Operators.ConcatenateObject(originalString, str);
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj3, obj7, ref obj3));
            }
            return RuntimeHelpers.GetObjectValue(originalString);
        }

        public object StripSlashes(string str)
        {
            if (Strings.Len(str) != 0)
            {
                str = Strings.Replace(str, @"\\", @"\", 1, -1, CompareMethod.Binary);
                str = Strings.Replace(str, @"\'", "'", 1, -1, CompareMethod.Binary);
            }
            return str;
        }
    }
}

