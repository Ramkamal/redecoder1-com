﻿namespace IPN
{
    using Microsoft.VisualBasic;
    using Microsoft.VisualBasic.CompilerServices;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class MD
    {
        private const int BITS_TO_A_BYTE = 8;
        private const int BITS_TO_A_WORD = 0x20;
        private const int BYTES_TO_A_WORD = 4;
        private object[] m_l2Power = new object[0x1f];
        private object[] m_lOnBits = new object[0x1f];

        public MD()
        {
            this.m_lOnBits[0] = 1L;
            this.m_lOnBits[1] = 3L;
            this.m_lOnBits[2] = 7L;
            this.m_lOnBits[3] = 15L;
            this.m_lOnBits[4] = 0x1fL;
            this.m_lOnBits[5] = 0x3fL;
            this.m_lOnBits[6] = 0x7fL;
            this.m_lOnBits[7] = 0xffL;
            this.m_lOnBits[8] = 0x1ffL;
            this.m_lOnBits[9] = 0x3ffL;
            this.m_lOnBits[10] = 0x7ffL;
            this.m_lOnBits[11] = 0xfffL;
            this.m_lOnBits[12] = 0x1fffL;
            this.m_lOnBits[13] = 0x3fffL;
            this.m_lOnBits[14] = 0x7fffL;
            this.m_lOnBits[15] = 0xffffL;
            this.m_lOnBits[0x10] = 0x1ffffL;
            this.m_lOnBits[0x11] = 0x3ffffL;
            this.m_lOnBits[0x12] = 0x7ffffL;
            this.m_lOnBits[0x13] = 0xfffffL;
            this.m_lOnBits[20] = 0x1fffffL;
            this.m_lOnBits[0x15] = 0x3fffffL;
            this.m_lOnBits[0x16] = 0x7fffffL;
            this.m_lOnBits[0x17] = 0xffffffL;
            this.m_lOnBits[0x18] = 0x1ffffffL;
            this.m_lOnBits[0x19] = 0x3ffffffL;
            this.m_lOnBits[0x1a] = 0x7ffffffL;
            this.m_lOnBits[0x1b] = 0xfffffffL;
            this.m_lOnBits[0x1c] = 0x1fffffffL;
            this.m_lOnBits[0x1d] = 0x3fffffffL;
            this.m_lOnBits[30] = 0x7fffffffL;
            this.m_l2Power[0] = 1L;
            this.m_l2Power[1] = 2L;
            this.m_l2Power[2] = 4L;
            this.m_l2Power[3] = 8L;
            this.m_l2Power[4] = 0x10L;
            this.m_l2Power[5] = 0x20L;
            this.m_l2Power[6] = 0x40L;
            this.m_l2Power[7] = 0x80L;
            this.m_l2Power[8] = 0x100L;
            this.m_l2Power[9] = 0x200L;
            this.m_l2Power[10] = 0x400L;
            this.m_l2Power[11] = 0x800L;
            this.m_l2Power[12] = 0x1000L;
            this.m_l2Power[13] = 0x2000L;
            this.m_l2Power[14] = 0x4000L;
            this.m_l2Power[15] = 0x8000L;
            this.m_l2Power[0x10] = 0x10000L;
            this.m_l2Power[0x11] = 0x20000L;
            this.m_l2Power[0x12] = 0x40000L;
            this.m_l2Power[0x13] = 0x80000L;
            this.m_l2Power[20] = 0x100000L;
            this.m_l2Power[0x15] = 0x200000L;
            this.m_l2Power[0x16] = 0x400000L;
            this.m_l2Power[0x17] = 0x800000L;
            this.m_l2Power[0x18] = 0x1000000L;
            this.m_l2Power[0x19] = 0x2000000L;
            this.m_l2Power[0x1a] = 0x4000000L;
            this.m_l2Power[0x1b] = 0x8000000L;
            this.m_l2Power[0x1c] = 0x10000000L;
            this.m_l2Power[0x1d] = 0x20000000L;
            this.m_l2Power[30] = 0x40000000L;
        }

        private object AddUnsigned(object lX, object lY)
        {
            object right = Operators.AndObject(lX, -2147483648);
            object obj7 = Operators.AndObject(lY, -2147483648);
            object left = Operators.AndObject(lX, 0x40000000);
            object obj6 = Operators.AndObject(lY, 0x40000000);
            object obj3 = Operators.AddObject(Operators.AndObject(lX, 0x3fffffff), Operators.AndObject(lY, 0x3fffffff));
            if (Conversions.ToBoolean(Operators.AndObject(left, obj6)))
            {
                obj3 = Operators.XorObject(Operators.XorObject(Operators.XorObject(obj3, -2147483648), right), obj7);
            }
            else if (Conversions.ToBoolean(Operators.OrObject(left, obj6)))
            {
                if (Conversions.ToBoolean(Operators.AndObject(obj3, 0x40000000)))
                {
                    obj3 = Operators.XorObject(Operators.XorObject(Operators.XorObject(obj3, -1073741824), right), obj7);
                }
                else
                {
                    obj3 = Operators.XorObject(Operators.XorObject(Operators.XorObject(obj3, 0x40000000), right), obj7);
                }
            }
            else
            {
                obj3 = Operators.XorObject(Operators.XorObject(obj3, right), obj7);
            }
            return RuntimeHelpers.GetObjectValue(obj3);
        }

        private object ConvertToWordArray(object sMessage)
        {
            object obj6;
            object obj9;
            object obj7 = Information.VarType(RuntimeHelpers.GetObjectValue(sMessage));
            object left = obj7;
            if (Operators.ConditionalCompareObjectEqual(left, 8, false))
            {
                obj6 = Strings.Len(RuntimeHelpers.GetObjectValue(sMessage));
            }
            else if (Operators.ConditionalCompareObjectEqual(left, 0x2011, false))
            {
                obj6 = Marshal.SizeOf(RuntimeHelpers.GetObjectValue(sMessage));
            }
            else
            {
                Information.Err().Raise(-1, "MD5", "Unknown Type passed to MD5 function", null, null);
            }
            object obj8 = Operators.MultiplyObject(Operators.AddObject(Operators.IntDivideObject(Operators.AddObject(obj6, 8), 0x40), 1), 0x10);
            object[] objArray = new object[Conversions.ToInteger(Operators.SubtractObject(obj8, 1)) + 1];
            object obj4 = 0;
            object obj3 = 0;
            while (Operators.ConditionalCompareObjectLess(obj3, obj6, false))
            {
                object obj5;
                obj9 = Operators.IntDivideObject(obj3, 4);
                obj4 = Operators.MultiplyObject(Operators.ModObject(obj3, 4), 8);
                object obj11 = obj7;
                if (Operators.ConditionalCompareObjectEqual(obj11, 8, false))
                {
                    obj5 = Strings.Asc(Strings.Mid(Conversions.ToString(sMessage), Conversions.ToInteger(Operators.AddObject(obj3, 1)), 1));
                }
                else if (Operators.ConditionalCompareObjectEqual(obj11, 0x2011, false))
                {
                    obj5 = Strings.Asc(Strings.Mid(Conversions.ToString(sMessage), Conversions.ToInteger(Operators.AddObject(obj3, 1)), 1));
                }
                objArray[Conversions.ToInteger(obj9)] = Operators.OrObject(objArray[Conversions.ToInteger(obj9)], this.LShift(RuntimeHelpers.GetObjectValue(obj5), RuntimeHelpers.GetObjectValue(obj4)));
                obj3 = Operators.AddObject(obj3, 1);
            }
            obj9 = Operators.IntDivideObject(obj3, 4);
            obj4 = Operators.MultiplyObject(Operators.ModObject(obj3, 4), 8);
            objArray[Conversions.ToInteger(obj9)] = Operators.OrObject(objArray[Conversions.ToInteger(obj9)], this.LShift(0x80, RuntimeHelpers.GetObjectValue(obj4)));
            objArray[Conversions.ToInteger(Operators.SubtractObject(obj8, 2))] = RuntimeHelpers.GetObjectValue(this.LShift(RuntimeHelpers.GetObjectValue(obj6), 3));
            objArray[Conversions.ToInteger(Operators.SubtractObject(obj8, 1))] = RuntimeHelpers.GetObjectValue(this.RShift(RuntimeHelpers.GetObjectValue(obj6), 0x1d));
            return objArray;
        }

        private object F(object x, object y, object z)
        {
            return Operators.OrObject(Operators.AndObject(x, y), Operators.AndObject(Operators.NotObject(x), z));
        }

        private void FF(object a, object b, object c, object d, object x, object s, object ac)
        {
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.F(RuntimeHelpers.GetObjectValue(b), RuntimeHelpers.GetObjectValue(c), RuntimeHelpers.GetObjectValue(d))), RuntimeHelpers.GetObjectValue(x))), RuntimeHelpers.GetObjectValue(ac)))));
            a = RuntimeHelpers.GetObjectValue(this.RotateLeft(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(s)));
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(b)));
        }

        private object G(object x, object y, object z)
        {
            return Operators.OrObject(Operators.AndObject(x, z), Operators.AndObject(y, Operators.NotObject(z)));
        }

        private void GG(object a, object b, object c, object d, object x, object s, object ac)
        {
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.G(RuntimeHelpers.GetObjectValue(b), RuntimeHelpers.GetObjectValue(c), RuntimeHelpers.GetObjectValue(d))), RuntimeHelpers.GetObjectValue(x))), RuntimeHelpers.GetObjectValue(ac)))));
            a = RuntimeHelpers.GetObjectValue(this.RotateLeft(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(s)));
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(b)));
        }

        private object H(object x, object y, object z)
        {
            return Operators.XorObject(Operators.XorObject(x, y), z);
        }

        private void HH(object a, object b, object c, object d, object x, object s, object ac)
        {
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.H(RuntimeHelpers.GetObjectValue(b), RuntimeHelpers.GetObjectValue(c), RuntimeHelpers.GetObjectValue(d))), RuntimeHelpers.GetObjectValue(x))), RuntimeHelpers.GetObjectValue(ac)))));
            a = RuntimeHelpers.GetObjectValue(this.RotateLeft(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(s)));
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(b)));
        }

        private object I(object x, object y, object z)
        {
            return Operators.XorObject(y, Operators.OrObject(x, Operators.NotObject(z)));
        }

        private void II(object a, object b, object c, object d, object x, object s, object ac)
        {
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(this.I(RuntimeHelpers.GetObjectValue(b), RuntimeHelpers.GetObjectValue(c), RuntimeHelpers.GetObjectValue(d))), RuntimeHelpers.GetObjectValue(x))), RuntimeHelpers.GetObjectValue(ac)))));
            a = RuntimeHelpers.GetObjectValue(this.RotateLeft(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(s)));
            a = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(a), RuntimeHelpers.GetObjectValue(b)));
        }

        private object LShift(object lValue, object iShiftBits)
        {
            if (Operators.ConditionalCompareObjectEqual(iShiftBits, 0, false))
            {
                return RuntimeHelpers.GetObjectValue(lValue);
            }
            if (Operators.ConditionalCompareObjectEqual(iShiftBits, 0x1f, false))
            {
                if (Conversions.ToBoolean(Operators.AndObject(lValue, 1)))
                {
                    return -2147483648;
                }
                return 0;
            }
            if (Conversions.ToBoolean(Operators.OrObject(Operators.CompareObjectLess(iShiftBits, 0, false), Operators.CompareObjectGreater(iShiftBits, 0x1f, false))))
            {
                Information.Err().Raise(6, null, null, null, null);
            }
            if (Conversions.ToBoolean(Operators.AndObject(lValue, this.m_l2Power[Conversions.ToInteger(Operators.SubtractObject(0x1f, iShiftBits))])))
            {
                return Operators.OrObject(Operators.MultiplyObject(Operators.AndObject(lValue, this.m_lOnBits[Conversions.ToInteger(Operators.SubtractObject(0x1f, Operators.AddObject(iShiftBits, 1)))]), this.m_l2Power[Conversions.ToInteger(iShiftBits)]), -2147483648);
            }
            return Operators.MultiplyObject(Operators.AndObject(lValue, this.m_lOnBits[Conversions.ToInteger(Operators.SubtractObject(0x1f, iShiftBits))]), this.m_l2Power[Conversions.ToInteger(iShiftBits)]);
        }

        public object MD5(object sMessage)
        {
            object obj10;
            object obj15;
            object objectValue = RuntimeHelpers.GetObjectValue(this.ConvertToWordArray(RuntimeHelpers.GetObjectValue(sMessage)));
            object obj2 = 0x67452301;
            object obj4 = -271733879;
            object obj6 = -1732584194;
            object obj8 = 0x10325476;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj10, 0, Information.UBound((Array) objectValue, 1), 0x10, ref obj15, ref obj10))
            {
                do
                {
                    object obj3 = RuntimeHelpers.GetObjectValue(obj2);
                    object obj5 = RuntimeHelpers.GetObjectValue(obj4);
                    object obj7 = RuntimeHelpers.GetObjectValue(obj6);
                    object obj9 = RuntimeHelpers.GetObjectValue(obj8);
                    this.FF(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 0) }, null)), 7, -680876936);
                    this.FF(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 1) }, null)), 12, -389564586);
                    this.FF(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 2) }, null)), 0x11, 0x242070db);
                    this.FF(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 3) }, null)), 0x16, -1044525330);
                    this.FF(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 4) }, null)), 7, -176418897);
                    this.FF(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 5) }, null)), 12, 0x4787c62a);
                    this.FF(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 6) }, null)), 0x11, -1473231341);
                    this.FF(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 7) }, null)), 0x16, -45705983);
                    this.FF(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 8) }, null)), 7, 0x698098d8);
                    this.FF(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 9) }, null)), 12, -1958414417);
                    this.FF(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 10) }, null)), 0x11, -42063);
                    this.FF(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 11) }, null)), 0x16, -1990404162);
                    this.FF(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 12) }, null)), 7, 0x6b901122);
                    this.FF(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 13) }, null)), 12, -40341101);
                    this.FF(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 14) }, null)), 0x11, -1502002290);
                    this.FF(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 15) }, null)), 0x16, 0x49b40821);
                    this.GG(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 1) }, null)), 5, -165796510);
                    this.GG(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 6) }, null)), 9, -1069501632);
                    this.GG(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 11) }, null)), 14, 0x265e5a51);
                    this.GG(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 0) }, null)), 20, -373897302);
                    this.GG(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 5) }, null)), 5, -701558691);
                    this.GG(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 10) }, null)), 9, 0x2441453);
                    this.GG(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 15) }, null)), 14, -660478335);
                    this.GG(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 4) }, null)), 20, -405537848);
                    this.GG(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 9) }, null)), 5, 0x21e1cde6);
                    this.GG(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 14) }, null)), 9, -1019803690);
                    this.GG(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 3) }, null)), 14, -187363961);
                    this.GG(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 8) }, null)), 20, 0x455a14ed);
                    this.GG(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 13) }, null)), 5, -1444681467);
                    this.GG(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 2) }, null)), 9, -51403784);
                    this.GG(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 7) }, null)), 14, 0x676f02d9);
                    this.GG(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 12) }, null)), 20, -1926607734);
                    this.HH(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 5) }, null)), 4, -378558);
                    this.HH(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 8) }, null)), 11, -2022574463);
                    this.HH(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 11) }, null)), 0x10, 0x6d9d6122);
                    this.HH(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 14) }, null)), 0x17, -35309556);
                    this.HH(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 1) }, null)), 4, -1530992060);
                    this.HH(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 4) }, null)), 11, 0x4bdecfa9);
                    this.HH(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 7) }, null)), 0x10, -155497632);
                    this.HH(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 10) }, null)), 0x17, -1094730640);
                    this.HH(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 13) }, null)), 4, 0x289b7ec6);
                    this.HH(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 0) }, null)), 11, -358537222);
                    this.HH(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 3) }, null)), 0x10, -722521979);
                    this.HH(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 6) }, null)), 0x17, 0x4881d05);
                    this.HH(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 9) }, null)), 4, -640364487);
                    this.HH(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 12) }, null)), 11, -421815835);
                    this.HH(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 15) }, null)), 0x10, 0x1fa27cf8);
                    this.HH(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 2) }, null)), 0x17, -995338651);
                    this.II(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 0) }, null)), 6, -198630844);
                    this.II(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 7) }, null)), 10, 0x432aff97);
                    this.II(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 14) }, null)), 15, -1416354905);
                    this.II(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 5) }, null)), 0x15, -57434055);
                    this.II(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 12) }, null)), 6, 0x655b59c3);
                    this.II(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 3) }, null)), 10, -1894986606);
                    this.II(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 10) }, null)), 15, -1051523);
                    this.II(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 1) }, null)), 0x15, -2054922799);
                    this.II(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 8) }, null)), 6, 0x6fa87e4f);
                    this.II(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 15) }, null)), 10, -30611744);
                    this.II(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 6) }, null)), 15, -1560198380);
                    this.II(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 13) }, null)), 0x15, 0x4e0811a1);
                    this.II(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 4) }, null)), 6, -145523070);
                    this.II(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 11) }, null)), 10, -1120210379);
                    this.II(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 2) }, null)), 15, 0x2ad7d2bb);
                    this.II(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(NewLateBinding.LateIndexGet(objectValue, new object[] { Operators.AddObject(obj10, 9) }, null)), 0x15, -343485551);
                    obj2 = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(obj2), RuntimeHelpers.GetObjectValue(obj3)));
                    obj4 = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(obj4), RuntimeHelpers.GetObjectValue(obj5)));
                    obj6 = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(obj6), RuntimeHelpers.GetObjectValue(obj7)));
                    obj8 = RuntimeHelpers.GetObjectValue(this.AddUnsigned(RuntimeHelpers.GetObjectValue(obj8), RuntimeHelpers.GetObjectValue(obj9)));
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj10, obj15, ref obj10));
            }
            return RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(null, typeof(Strings), "LCase", new object[] { Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(this.WordToHex(RuntimeHelpers.GetObjectValue(obj2)), this.WordToHex(RuntimeHelpers.GetObjectValue(obj4))), this.WordToHex(RuntimeHelpers.GetObjectValue(obj6))), this.WordToHex(RuntimeHelpers.GetObjectValue(obj8))) }, null, null, null));
        }

        private object RotateLeft(object lValue, object iShiftBits)
        {
            return Operators.OrObject(this.LShift(RuntimeHelpers.GetObjectValue(lValue), RuntimeHelpers.GetObjectValue(iShiftBits)), this.RShift(RuntimeHelpers.GetObjectValue(lValue), Operators.SubtractObject(0x20, iShiftBits)));
        }

        private object RShift(object lValue, object iShiftBits)
        {
            if (Operators.ConditionalCompareObjectEqual(iShiftBits, 0, false))
            {
                return RuntimeHelpers.GetObjectValue(lValue);
            }
            if (Operators.ConditionalCompareObjectEqual(iShiftBits, 0x1f, false))
            {
                if (Conversions.ToBoolean(Operators.AndObject(lValue, -2147483648)))
                {
                    return 1;
                }
                return 0;
            }
            if (Conversions.ToBoolean(Operators.OrObject(Operators.CompareObjectLess(iShiftBits, 0, false), Operators.CompareObjectGreater(iShiftBits, 0x1f, false))))
            {
                Information.Err().Raise(6, null, null, null, null);
            }
            object left = Operators.IntDivideObject(Operators.AndObject(lValue, 0x7ffffffe), this.m_l2Power[Conversions.ToInteger(iShiftBits)]);
            if (Conversions.ToBoolean(Operators.AndObject(lValue, -2147483648)))
            {
                left = Operators.OrObject(left, Operators.IntDivideObject(0x40000000, this.m_l2Power[Conversions.ToInteger(Operators.SubtractObject(iShiftBits, 1))]));
            }
            return left;
        }

        private object WordToHex(object lValue)
        {
            object obj3;
            object obj4;
            object obj7;
            if (ObjectFlowControl.ForLoopControl.ForLoopInitObj(obj3, 0, 3, 1, ref obj7, ref obj3))
            {
                do
                {
                    object obj2 = Operators.AndObject(this.RShift(RuntimeHelpers.GetObjectValue(lValue), Operators.MultiplyObject(obj3, 8)), this.m_lOnBits[7]);
                    obj4 = Operators.ConcatenateObject(obj4, Strings.Right("0" + Conversion.Hex(RuntimeHelpers.GetObjectValue(obj2)), 2));
                }
                while (ObjectFlowControl.ForLoopControl.ForNextCheckObj(obj3, obj7, ref obj3));
            }
            return obj4;
        }
    }
}

