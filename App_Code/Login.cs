﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public static class Login
{
    public static void CheckCredential(string username, string password, ref int id, ref string usertype)
    {
        SqlCommand sQL = new SqlCommand("uspCheckUserCredentials_urbb")
        {
            CommandType = CommandType.StoredProcedure
        };
        sQL.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = Convert.ToString(username).Trim();
        sQL.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = Convert.ToString(password).Trim();
        DataSet set = new DataSet();
        set = CommonFunctions.ReturnDatasource(sQL);
        if ((set.Tables.Count > 0) && (set.Tables[0].Rows.Count > 0))
        {
            usertype = Convert.ToString(set.Tables[0].Rows[0]["UserType"]).ToUpper().Trim();
            id = Convert.ToInt32(set.Tables[0].Rows[0]["Id"]);
            HttpContext.Current.Session["uname"] = username;
            HttpContext.Current.Session["pwd"] = password;
            HttpContext.Current.Session["id"] = Convert.ToString((int) id);
            HttpContext.Current.Session["type"] = Convert.ToString((string) usertype);
            HttpContext.Current.Session["AffiliateID"] = Convert.ToString(set.Tables[0].Rows[0]["AffiliateID"]).ToUpper().Trim();
        }
        else
        {
            id = 0;
            usertype = "";
        }
    }

    public static void CheckUserCredentialOnLoad(ref int id, ref string usertype)
    {
        string str;
        string str2;
        if ((HttpContext.Current.Session["uname"] != null) && (HttpContext.Current.Session["pwd"] != null))
        {
            str = Convert.ToString(HttpContext.Current.Session["uname"]);
            str2 = Convert.ToString(HttpContext.Current.Session["pwd"]);
        }
        else
        {
            return;
        }
        CheckCredential(str, str2, ref id, ref usertype);
        if ((id == 0) && (usertype == ""))
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Response.Redirect("~/Forms/Login.aspx");
        }
    }

    public static string GetAffiliate()
    {
        return Convert.ToString(HttpContext.Current.Session["AffiliateID"]);
    }

    public static string GetId()
    {
        return Convert.ToString(CommonFunctions.ChangeToZero(HttpContext.Current.Session["id"]));
    }

    public static string GetPassword()
    {
        return Convert.ToString(HttpContext.Current.Session["pwd"]);
    }

    public static string GetUsername()
    {
        return Convert.ToString(HttpContext.Current.Session["uname"]);
    }

    public static string GetUserType()
    {
        return Convert.ToString(HttpContext.Current.Session["type"]);
    }

    public static bool IsEmailExists(string emailid, int ClientID)
    {
        SqlCommand oledbCmd = new SqlCommand("uspCheckEmailExists_rwxm")
        {
            CommandType = CommandType.StoredProcedure
        };
        oledbCmd.Parameters.Add("@EmailId", SqlDbType.VarChar, 150).Value = emailid.Trim();
        oledbCmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = ClientID;
        return CommonFunctions.IsRecordExists(oledbCmd);
    }

    public static bool IsUsernameExists(string username, int ClientID, int EmployeeId, int TeamId)
    {
        SqlCommand oledbCmd = new SqlCommand("uspCheckUsernameExists_zqwm")
        {
            CommandType = CommandType.StoredProcedure
        };
        oledbCmd.Parameters.Add("@username", SqlDbType.VarChar, 50).Value = username.Trim();
        oledbCmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = ClientID;
        oledbCmd.Parameters.Add("@EmployeeId", SqlDbType.Int).Value = EmployeeId;
        oledbCmd.Parameters.Add("@TeamId", SqlDbType.Int).Value = TeamId;
        return CommonFunctions.IsRecordExists(oledbCmd);
    }

    public static void LogOut()
    {
        HttpContext.Current.Session.Clear();
    }
}

