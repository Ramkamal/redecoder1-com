﻿namespace MK
{
    using iTextSharp.text;
    using iTextSharp.text.html;
    using iTextSharp.text.pdf;
    using Microsoft.VisualBasic;
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    public static class CommonFunctions
    {
        public static string ChangeNullToString(object strValue)
        {
            if ((strValue is DBNull) || (strValue == null))
            {
                return "";
            }
            return Convert.ToString(strValue);
        }

        public static long ChangeToLong(object txtValue)
        {
            if (txtValue == null)
            {
                return 0L;
            }
            if (txtValue.ToString() == "")
            {
                return 0L;
            }
            return Convert.ToInt64(txtValue);
        }

        public static int ChangeToZero(object txtValue)
        {
            if (txtValue == null)
            {
                return 0;
            }
            if (txtValue.ToString() == "")
            {
                return 0;
            }
            return Convert.ToInt32(txtValue);
        }

        public static string ChangeToZero(string value, bool returnNull)
        {
            value = value.Trim();
            if (value == "")
            {
                if (returnNull)
                {
                    return "NULL";
                }
                return "0";
            }
            return value;
        }

        public static double ChangeToZero_Dbl(object txtValue)
        {
            if (txtValue == null)
            {
                return 0.0;
            }
            if (txtValue.ToString() == "")
            {
                return 0.0;
            }
            return Convert.ToDouble(txtValue);
        }

        public static void ClearFields(Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                if (control is TextBox)
                {
                    TextBox box = (TextBox) control;
                    box.Text = "";
                }
                else if (control is HtmlInputHidden)
                {
                    HtmlInputHidden hidden = (HtmlInputHidden) control;
                    hidden.Value = "";
                }
                else if (control is DropDownList)
                {
                    ((DropDownList) control).ClearSelection();
                }
                else if (control is ListBox)
                {
                    ((ListBox) control).ClearSelection();
                }
                else if (control is RadioButtonList)
                {
                    ((RadioButtonList) control).ClearSelection();
                }
                else if (control is CheckBox)
                {
                    CheckBox box3 = (CheckBox) control;
                    box3.Checked = false;
                }
                else if (control is CheckBoxList)
                {
                    ((CheckBoxList) control).ClearSelection();
                }
                if (control.HasControls())
                {
                    ClearFields(control);
                }
            }
        }

        public static void ClearTextBoxesInPage(Page p)
        {
            foreach (Control control in p.Controls)
            {
                if (control is TextBox)
                {
                    ((TextBox) control).Text = "";
                }
            }
        }

        public static string ConvertBitToYesNo(bool b)
        {
            if (b)
            {
                return "Yes";
            }
            return "No";
        }

        public static string ConvertToDateMonthFormat(string date)
        {
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public static string ConvertToMonthDateFormat(string date)
        {
            DateTimeFormatInfo provider = new DateTimeFormatInfo {
                ShortDatePattern = "dd/MM/yyyy"
            };
            return Convert.ToDateTime(date, provider).ToShortDateString().ToString();
        }

        public static int ConvertYesNoToBit(string str)
        {
            if (str == null)
            {
                return -1;
            }
            if (str.ToUpper().Trim() == "YES")
            {
                return 1;
            }
            return 0;
        }

        public static void DisplayControl(HtmlControl c)
        {
            c.Attributes.Add("style", "display:block");
        }

        public static void DisplayControl(WebControl c)
        {
            c.Attributes.Add("style", "display:block");
        }

        public static void Downloadfile(string filename)
        {
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filename));
            HttpContext.Current.Response.TransmitFile(filename);
            HttpContext.Current.Response.End();
        }

        public static bool ExecuteNonQuery(SqlCommand oledbCmd)
        {
            SqlConnection connection = null;
            bool flag;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                oledbCmd.Connection = connection;
                oledbCmd.ExecuteNonQuery();
                oledbCmd.Dispose();
                connection.Close();
                connection.Dispose();
                flag = true;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (oledbCmd != null)
                {
                    oledbCmd.Dispose();
                }
                throw exception;
            }
            return flag;
        }

        public static bool ExecuteNonQuery(string SQL)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            bool flag;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                command = new SqlCommand(SQL, connection);
                command.ExecuteNonQuery();
                command.Dispose();
                connection.Close();
                connection.Dispose();
                flag = true;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                throw exception;
            }
            return flag;
        }

        public static bool ExecuteNonQuery(SqlCommand oledbCmd, ref Label lbl)
        {
            SqlConnection connection = null;
            bool flag;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                oledbCmd.Connection = connection;
                oledbCmd.ExecuteNonQuery();
                oledbCmd.Dispose();
                connection.Close();
                connection.Dispose();
                flag = true;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (oledbCmd != null)
                {
                    oledbCmd.Dispose();
                }
                lbl.Text = exception.Message;
                throw exception;
            }
            return flag;
        }

        public static bool ExecuteNonQuery(string SQL, ref Label lbl)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            bool flag;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                command = new SqlCommand(SQL, connection);
                command.ExecuteNonQuery();
                command.Dispose();
                connection.Close();
                connection.Dispose();
                flag = true;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                lbl.Text = exception.Message;
                throw exception;
            }
            return flag;
        }

        public static string ExecuteScalar(SqlCommand oledbCmd)
        {
            SqlConnection connection = null;
            string str2;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                oledbCmd.Connection = connection;
                string str = Convert.ToString(oledbCmd.ExecuteScalar());
                oledbCmd.Dispose();
                connection.Close();
                connection.Dispose();
                str2 = str;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (oledbCmd != null)
                {
                    oledbCmd.Dispose();
                }
                throw exception;
            }
            return str2;
        }

        public static string ExecuteScalar(string SQL)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            string str2;
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                command = new SqlCommand(SQL, connection);
                string str = command.ExecuteScalar().ToString();
                command.Dispose();
                connection.Close();
                connection.Dispose();
                str2 = str;
            }
            catch (Exception exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                throw exception;
            }
            return str2;
        }

        public static void ExportToCSV(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.AddHeader("content-Disposition", "attachement;filename=" + fileName);
            HttpContext.Current.Response.ContentType = "text/.csv";
            using (StreamWriter writer = new StreamWriter(HttpContext.Current.Response.OutputStream))
            {
                writer.WriteLine(GetCSVLine(gv.HeaderRow.Cells));
                foreach (GridViewRow row in gv.Rows)
                {
                    writer.WriteLine(GetCSVLine(row.Cells));
                }
            }
            HttpContext.Current.Response.End();
        }

        public static void ExportToExcel(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            using (StringWriter writer = new StringWriter())
            {
                using (HtmlTextWriter writer2 = new HtmlTextWriter(writer))
                {
                    System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
                    PrepareControl(gv).RenderControl(writer2);
                    HttpContext.Current.Response.Write(writer.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        public static void ExportToHTML(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            using (StringWriter writer = new StringWriter())
            {
                using (HtmlTextWriter writer2 = new HtmlTextWriter(writer))
                {
                    System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
                    PrepareControl(gv).RenderControl(writer2);
                    HttpContext.Current.Response.Write(writer.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        public static void ExportToPDF(Control gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=a.pdf");
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            PrepareControlForExport(ref gv);
            gv.RenderControl(writer2);
            string str = "<html><body>" + writer.ToString() + "</body></html>";
            Document document = new Document(PageSize.A3, 80f, 50f, 30f, 65f);
            PdfWriter instance = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            string tempFileName = Path.GetTempFileName();
            StreamWriter writer4 = new StreamWriter(tempFileName, false);
            writer4.Write(str);
            writer4.Close();
            HtmlParser.Parse(document, tempFileName);
            document.Close();
            instance.Close();
            System.IO.File.Delete(tempFileName);
            HttpContext.Current.Response.End();
        }

        public static void ExportToPDF(GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/pdf";
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
            table = PrepareControl(gv);
            table.RenderControl(writer2);
            table.RenderControl(writer2);
            string str = "<html><body>" + writer.ToString() + "</body></html>";
            Document document = new Document(PageSize.A3, 80f, 50f, 30f, 65f);
            PdfWriter instance = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            string tempFileName = Path.GetTempFileName();
            StreamWriter writer4 = new StreamWriter(tempFileName, false);
            writer4.Write(str);
            writer4.Close();
            HtmlParser.Parse(document, tempFileName);
            document.Close();
            instance.Close();
            System.IO.File.Delete(tempFileName);
            HttpContext.Current.Response.End();
        }

        public static void ExportToWord(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
            PrepareControl(gv).RenderControl(writer2);
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            HttpContext.Current.Response.ContentEncoding = Encoding.UTF7;
            HttpContext.Current.Response.ContentType = "application/vnd.word";
            HttpContext.Current.Response.Output.Write(writer.ToString());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public static string GenerateRandomCode()
        {
            Random random = new Random();
            string str = "";
            for (int i = 0; i < 6; i++)
            {
                str = str + random.Next(10).ToString();
            }
            return str;
        }

        public static string GetCSVLine(TableCellCollection cellsToAdd)
        {
            string str = string.Empty;
            bool flag = true;
            foreach (TableCell cell in cellsToAdd)
            {
                if (!flag)
                {
                    str = str + ",";
                }
                flag = false;
                str = str + "\"" + HttpContext.Current.Server.HtmlDecode(cell.Text).Replace("\"", "\"\"") + "\"";
            }
            return str;
        }

        public static DateTime GetDateTime()
        {
            DateTime result = new DateTime();
            HttpCookie cookie = HttpContext.Current.Request.Cookies["ClientDateTime"];
            if (cookie != null)
            {
                DateTime.TryParse(cookie.Value, out result);
            }
            return result;
        }

        public static string GetTimeStamp()
        {
            return DateTime.Now.ToString().Replace(" ", "").Replace("-", "").Replace(",", "").Replace("/", "").Replace(@"\", "").Replace(" ", "").Replace(":", "");
        }

        public static int GetTimeZoneOffset()
        {
            int totalMinutes = (int) TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalMinutes;
            HttpCookie cookie = HttpContext.Current.Request.Cookies["ClientTimeZone"];
            if (cookie != null)
            {
                int.TryParse(cookie.Value, out totalMinutes);
            }
            return totalMinutes;
        }

        public static string GetValuwithCommaFromList(object lsts)
        {
            ListControl control = (ListControl) lsts;
            string expression = "";
            for (int i = 0; i <= (control.Items.Count - 1); i++)
            {
                if (control.Items[i].Selected)
                {
                    expression = expression + control.Items[i].Value + ",";
                }
            }
            if (Strings.Len(expression) > 0)
            {
                expression = Strings.Left(expression, Strings.Len(expression) - 1);
            }
            return expression;
        }

        public static void HideControl(HtmlControl c)
        {
            c.Attributes.Add("style", "display:none");
        }

        public static void HideControl(WebControl c)
        {
            c.Attributes.Add("style", "display:none");
        }

        public static bool IsDate(string txt)
        {
            txt = txt.Trim();
            if (txt.Trim() != "")
            {
                if (!Regex.Match(txt, @"([1-9]|0[1-9]|[12][0-9]|3[01])/(0[1-9]|[1-9]|1[012])/(19|20)\d\d").Success)
                {
                    return Regex.Match(txt, @"(([1-9]|0[1-9]|1[012])/[1-9]|0[1-9]|[12][0-9]|3[01])/(19|20)\d\d").Success;
                }
                return true;
            }
            return false;
        }

        public static bool IsMoney(string txt)
        {
            txt = txt.Trim();
            if (((txt.Trim() != "") && !Regex.Match(txt, @"^\d+$").Success) && !Regex.Match(txt, @"^\d{1,}.\d\d$").Success)
            {
                return false;
            }
            return true;
        }

        public static bool IsNumber(string txt)
        {
            txt = txt.Trim();
            if ((txt.Trim() != "") && !Regex.Match(txt, @"^\d+$").Success)
            {
                return false;
            }
            return true;
        }

        public static bool IsRecordExists(SqlCommand oledbCmd)
        {
            SqlConnection connection = null;
            SqlDataReader reader = null;
            bool flag2;
            try
            {
                bool flag;
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                oledbCmd.Connection = connection;
                reader = oledbCmd.ExecuteReader();
                if (reader.Read())
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                reader.Close();
                oledbCmd.Dispose();
                connection.Close();
                connection.Dispose();
                flag2 = flag;
            }
            catch (Exception exception)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (oledbCmd != null)
                {
                    oledbCmd.Dispose();
                }
                throw exception;
            }
            return flag2;
        }

        public static bool IsRecordExists(string SQL)
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            SqlDataReader reader = null;
            bool flag2;
            try
            {
                bool flag;
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                command = new SqlCommand(SQL, connection);
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                reader.Close();
                command.Dispose();
                connection.Close();
                connection.Dispose();
                flag2 = flag;
            }
            catch (Exception exception)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                throw exception;
            }
            return flag2;
        }

        public static void Logout()
        {
            try
            {
                HttpContext.Current.Response.Cookies["uname"].Expires = DateTime.Now.AddYears(-30);
                HttpContext.Current.Response.Cookies["pwd"].Expires = DateTime.Now.AddYears(-30);
                HttpContext.Current.Response.Cookies["isadmin"].Expires = DateTime.Now.AddYears(-30);
                HttpContext.Current.Response.Redirect("Index.aspx");
            }
            catch (Exception exception)
            {
                WriteError(exception);
            }
        }

        public static System.Web.UI.WebControls.Table PrepareControl(GridView gv)
        {
            System.Web.UI.WebControls.Table table = new System.Web.UI.WebControls.Table();
            if (gv.HeaderRow != null)
            {
                PrepareControlForExport(gv.HeaderRow);
                table.Rows.Add(gv.HeaderRow);
            }
            foreach (GridViewRow row in gv.Rows)
            {
                PrepareControlForExport(row);
                table.Rows.Add(row);
            }
            if (gv.FooterRow != null)
            {
                PrepareControlForExport(gv.FooterRow);
                table.Rows.Add(gv.FooterRow);
            }
            return table;
        }

        public static void PrepareControlForExport(ref Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control control2 = control.Controls[i];
                if (control2 is LinkButton)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as LinkButton).Text));
                }
                else if (control2 is ImageButton)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as ImageButton).AlternateText));
                }
                else if (control2 is HyperLink)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as HyperLink).Text));
                }
                else if (control2 is DropDownList)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as DropDownList).SelectedItem.Text));
                }
                else if (control2 is CheckBox)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as CheckBox).Checked ? "True" : "False"));
                }
                if (control2.HasControls())
                {
                    PrepareControlForExport(ref control2);
                }
            }
        }

        public static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control control2 = control.Controls[i];
                if (control2 is LinkButton)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as LinkButton).Text));
                }
                else if (control2 is ImageButton)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as ImageButton).AlternateText));
                }
                else if (control2 is HyperLink)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as HyperLink).Text));
                }
                else if (control2 is DropDownList)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as DropDownList).SelectedItem.Text));
                }
                else if (control2 is CheckBox)
                {
                    control.Controls.Remove(control2);
                    control.Controls.AddAt(i, new LiteralControl((control2 as CheckBox).Checked ? "True" : "False"));
                }
                if (control2.HasControls())
                {
                    PrepareControlForExport(control2);
                }
            }
        }

        public static string PrepareString(object strVariant)
        {
            if ((strVariant == null) || (Convert.ToString(strVariant).Trim() == ""))
            {
                return "''";
            }
            return Quoted(strVariant.ToString().Trim());
        }

        public static string PrepareString(object strVariant, bool blnReturnNull)
        {
            if (blnReturnNull && ((strVariant == null) || (Convert.ToString(strVariant).Trim() == "")))
            {
                return "Null";
            }
            if ((strVariant == null) || (Convert.ToString(strVariant).Trim() == ""))
            {
                return "''";
            }
            return Quoted(strVariant.ToString().Trim());
        }

        public static string Quoted(string strToBeQuoted)
        {
            return ("'" + strToBeQuoted.Replace("'", "''") + "'");
        }

        public static string RemoveLastCharacter(string str)
        {
            return Strings.Left(str, Strings.Len(str) - 1);
        }

        public static DataSet ReturnDatasource(SqlCommand SQL)
        {
            SqlDataAdapter adapter = null;
            SqlConnection connection = null;
            DataSet set2;
            DataSet dataSet = new DataSet();
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                SQL.Connection = connection;
                adapter = new SqlDataAdapter(SQL);
                adapter.Fill(dataSet);
                adapter.Dispose();
                SQL.Dispose();
                connection.Dispose();
                set2 = dataSet;
            }
            catch (Exception exception)
            {
                if (adapter != null)
                {
                    adapter.Dispose();
                }
                if (SQL != null)
                {
                    SQL.Dispose();
                }
                if (connection != null)
                {
                    connection.Dispose();
                }
                dataSet.Dispose();
                throw exception;
            }
            return set2;
        }

        public static DataSet ReturnDatasource(string SQL)
        {
            SqlDataAdapter adapter = null;
            DataSet set2;
            DataSet dataSet = new DataSet();
            try
            {
                adapter = new SqlDataAdapter(SQL, CommonVariables.connectionString);
                adapter.Fill(dataSet);
                adapter.Dispose();
                set2 = dataSet;
            }
            catch (Exception exception)
            {
                if (adapter != null)
                {
                    adapter.Dispose();
                }
                dataSet.Dispose();
                throw exception;
            }
            return set2;
        }

        public static string Savefile(FileUpload FU)
        {
            string str2;
            string filename = "";
            try
            {
                if (FU.HasFile && (FU.PostedFile != null))
                {
                    filename = HttpContext.Current.Server.MapPath(@"~\UploadData\") + GetTimeStamp() + Path.GetExtension(FU.PostedFile.FileName);
                    FU.SaveAs(filename);
                }
                str2 = filename;
            }
            catch (Exception exception)
            {
                WriteError(exception);
                throw exception;
            }
            return str2;
        }

        public static bool Sendmail(string from, string to, string body, string subject, bool isBodyHtml)
        {
            bool flag;
            try
            {
                //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["host"]) {
                //    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"])
                //};
                //MailMessage message = new MailMessage(from, to, subject, body) {
                //    IsBodyHtml = isBodyHtml
                //};

                SmtpClient client = new SmtpClient("smtp.gmail.com", 0x24b)
                {
                    Credentials = new NetworkCredential("redecodr@gmail.com", "Logme!n123")
                };
                MailMessage message = new MailMessage("redecodr@gmail.com", to, subject, body)
                {
                    IsBodyHtml = isBodyHtml
                };
                client.Send(message);
                flag = true;
            }
            catch (Exception exception)
            {
                WriteError(exception);
                throw exception;
            }
            return flag;
        }

        public static bool Sendmail(string from, string to, string body, string subject, bool isBodyHtml, string filename, bool delFile)
        {
            bool flag;
            try
            {
                //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["host"]) {
                //    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"])
                //};
                //MailMessage message = new MailMessage(from, to, subject, body) {
                //    IsBodyHtml = isBodyHtml
                //};

                SmtpClient client = new SmtpClient("smtp.gmail.com", 0x24b)
                {
                    Credentials = new NetworkCredential("redecodr@gmail.com", "Logme!n123")
                };
                MailMessage message = new MailMessage("redecodr@gmail.com", to, subject, body)
                {
                    IsBodyHtml = isBodyHtml
                };
                Attachment attachment = new Attachment(filename);
                client.Send(message);
                if (delFile)
                {
                    try
                    {
                        System.IO.File.Delete(filename);
                    }
                    catch
                    {
                    }
                }
                flag = true;
            }
            catch (Exception exception)
            {
                WriteError(exception);
                throw exception;
            }
            return flag;
        }

        public static void Showfile(string filename)
        {
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.TransmitFile(filename);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public static void WriteError(Exception ex)
        {
            try
            {
                if (ex.GetType().ToString() == "System.Threading.ThreadAbortException")
                {
                    return;
                }
            }
            catch
            {
            }
            SqlConnection connection = null;
            SqlCommand command = null;
            StringWriter writer = new StringWriter();
            try
            {
                connection = new SqlConnection(CommonVariables.connectionString);
                connection.Open();
                command = new SqlCommand {
                    Connection = connection
                };
                writer.WriteLine("Message        : " + ex.Message);
                writer.WriteLine("Source         : " + ex.Source);
                writer.WriteLine("StackTrace     : " + ex.StackTrace);
                writer.WriteLine("TargetSite     : " + ex.TargetSite);
                writer.WriteLine("InnerException : " + ex.InnerException);
                writer.WriteLine("HelpLink : " + ex.HelpLink);
                command.CommandText = "uspInsertError_lpvn";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("Error", SqlDbType.VarChar).Value = writer.ToString();
                command.ExecuteNonQuery();
                command.Dispose();
                connection.Close();
                connection.Dispose();
            }
            catch
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
            }
        }
    }
}

