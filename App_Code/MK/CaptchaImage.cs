﻿namespace MK
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;

    public class CaptchaImage
    {
        private string familyName;
        private int height;
        private Bitmap image;
        private Random random;
        private string text;
        private int width;

        public CaptchaImage(string s, int width, int height)
        {
            this.random = new Random();
            this.text = s;
            this.SetDimensions(width, height);
            this.GenerateImage();
        }

        public CaptchaImage(string s, int width, int height, string familyName)
        {
            this.random = new Random();
            this.text = s;
            this.SetDimensions(width, height);
            this.SetFamilyName(familyName);
            this.GenerateImage();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.image.Dispose();
            }
        }

        ~CaptchaImage()
        {
            this.Dispose(false);
        }

        private void GenerateImage()
        {
            SizeF ef;
            Font font;
            Bitmap image = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(image);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle rect = new Rectangle(0, 0, this.width, this.height);
            HatchBrush brush = new HatchBrush(HatchStyle.SmallConfetti, Color.LightGray, Color.White);
            graphics.FillRectangle(brush, rect);
            float emSize = rect.Height + 1;
            do
            {
                emSize--;
                font = new Font(this.familyName, emSize, FontStyle.Bold);
                ef = graphics.MeasureString(this.text, font);
            }
            while (ef.Width > rect.Width);
            StringFormat format = new StringFormat {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            GraphicsPath path = new GraphicsPath();
            path.AddString(this.text, font.FontFamily, (int) font.Style, font.Size, rect, format);
            float num2 = 4f;
            PointF[] destPoints = new PointF[] { new PointF(((float) this.random.Next(rect.Width)) / num2, ((float) this.random.Next(rect.Height)) / num2), new PointF(rect.Width - (((float) this.random.Next(rect.Width)) / num2), ((float) this.random.Next(rect.Height)) / num2), new PointF(((float) this.random.Next(rect.Width)) / num2, rect.Height - (((float) this.random.Next(rect.Height)) / num2)), new PointF(rect.Width - (((float) this.random.Next(rect.Width)) / num2), rect.Height - (((float) this.random.Next(rect.Height)) / num2)) };
            Matrix matrix = new Matrix();
            matrix.Translate(0f, 0f);
            path.Warp(destPoints, rect, matrix, WarpMode.Perspective, 0f);
            brush = new HatchBrush(HatchStyle.LargeConfetti, Color.LightGray, Color.DarkGray);
            graphics.FillPath(brush, path);
            int num3 = Math.Max(rect.Width, rect.Height);
            for (int i = 0; i < ((int) (((float) (rect.Width * rect.Height)) / 30f)); i++)
            {
                int x = this.random.Next(rect.Width);
                int y = this.random.Next(rect.Height);
                int width = this.random.Next(num3 / 50);
                int height = this.random.Next(num3 / 50);
                graphics.FillEllipse(brush, x, y, width, height);
            }
            font.Dispose();
            brush.Dispose();
            graphics.Dispose();
            this.image = image;
        }

        private void SetDimensions(int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.");
            }
            if (height <= 0)
            {
                throw new ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.");
            }
            this.width = width;
            this.height = height;
        }

        private void SetFamilyName(string familyName)
        {
            try
            {
                Font font = new Font(this.familyName, 12f);
                familyName = familyName;
                font.Dispose();
            }
            catch (Exception)
            {
                this.familyName = FontFamily.GenericSerif.Name;
            }
        }

        public int Height
        {
            get
            {
                return this.height;
            }
        }

        public Bitmap Image
        {
            get
            {
                return this.image;
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
        }

        public int Width
        {
            get
            {
                return this.width;
            }
        }
    }
}

