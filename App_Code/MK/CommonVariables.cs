﻿namespace MK
{
    using System;
    using System.Configuration;

    public static class CommonVariables
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["Constr"].ToString();
        public static string SystemErrorMsg = "Error on application, Please try again later";
    }
}

