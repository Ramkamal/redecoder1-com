﻿namespace API.Properties
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
    internal sealed class Settings : ApplicationSettingsBase
    {
        private static Settings defaultInstance = ((Settings) SettingsBase.Synchronized(new Settings()));

        [DebuggerNonUserCode, SpecialSetting(SpecialSetting.WebServiceUrl), DefaultSettingValue("http://69.72.147.245/Forms/captcha.asmx"), ApplicationScopedSetting]
        public string API_CAPTCHA_CAPTCHA
        {
            get
            {
                return (string) this["API_CAPTCHA_CAPTCHA"];
            }
        }

        public static Settings Default
        {
            get
            {
                return defaultInstance;
            }
        }
    }
}

