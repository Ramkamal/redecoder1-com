﻿namespace API.CAPTCHA
{
    using System;
    using System.CodeDom.Compiler;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void UploadCaptchaCompletedEventHandler(object sender, UploadCaptchaCompletedEventArgs e);
}

