﻿namespace API.CAPTCHA
{
    using API.Properties;
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Web.Services;
    using System.Web.Services.Description;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;

    [DesignerCategory("code"), DebuggerStepThrough, GeneratedCode("System.Web.Services", "4.0.30319.1"), WebServiceBinding(Name="CAPTCHASoap", Namespace="http://tempuri.org/")]
    public class CAPTCHA : SoapHttpClientProtocol
    {
        private SendOrPostCallback GetTextOperationCompleted;
        private SendOrPostCallback RequestBalanceOperationCompleted;
        private SendOrPostCallback SetBadImageOperationCompleted;
        private SendOrPostCallback UploadCaptchaOperationCompleted;
        private bool useDefaultCredentialsSetExplicitly;

        public event GetTextCompletedEventHandler GetTextCompleted;

        public event RequestBalanceCompletedEventHandler RequestBalanceCompleted;

        public event SetBadImageCompletedEventHandler SetBadImageCompleted;

        public event UploadCaptchaCompletedEventHandler UploadCaptchaCompleted;

        public CAPTCHA()
        {
            this.Url = Settings.Default.API_CAPTCHA_CAPTCHA;
            if (this.IsLocalFileSystemWebService(this.Url))
            {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else
            {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        public void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        [SoapDocumentMethod("http://tempuri.org/GetText", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public string GetText(string username, string password, int imageid)
        {
            return (string) base.Invoke("GetText", new object[] { username, password, imageid })[0];
        }

        public void GetTextAsync(string username, string password, int imageid)
        {
            this.GetTextAsync(username, password, imageid, null);
        }

        public void GetTextAsync(string username, string password, int imageid, object userState)
        {
            if (this.GetTextOperationCompleted == null)
            {
                this.GetTextOperationCompleted = new SendOrPostCallback(this.OnGetTextOperationCompleted);
            }
            base.InvokeAsync("GetText", new object[] { username, password, imageid }, this.GetTextOperationCompleted, userState);
        }

        private bool IsLocalFileSystemWebService(string url)
        {
            if ((url == null) || (url == string.Empty))
            {
                return false;
            }
            Uri uri = new Uri(url);
            return ((uri.Port >= 0x400) && (string.Compare(uri.Host, "localHost", StringComparison.OrdinalIgnoreCase) == 0));
        }

        private void OnGetTextOperationCompleted(object arg)
        {
            if (this.GetTextCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.GetTextCompleted(this, new GetTextCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OnRequestBalanceOperationCompleted(object arg)
        {
            if (this.RequestBalanceCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.RequestBalanceCompleted(this, new RequestBalanceCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OnSetBadImageOperationCompleted(object arg)
        {
            if (this.SetBadImageCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.SetBadImageCompleted(this, new AsyncCompletedEventArgs(args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OnUploadCaptchaOperationCompleted(object arg)
        {
            if (this.UploadCaptchaCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.UploadCaptchaCompleted(this, new UploadCaptchaCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        [SoapDocumentMethod("http://tempuri.org/RequestBalance", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public double RequestBalance(string username, string password)
        {
            return (double) base.Invoke("RequestBalance", new object[] { username, password })[0];
        }

        public void RequestBalanceAsync(string username, string password)
        {
            this.RequestBalanceAsync(username, password, null);
        }

        public void RequestBalanceAsync(string username, string password, object userState)
        {
            if (this.RequestBalanceOperationCompleted == null)
            {
                this.RequestBalanceOperationCompleted = new SendOrPostCallback(this.OnRequestBalanceOperationCompleted);
            }
            base.InvokeAsync("RequestBalance", new object[] { username, password }, this.RequestBalanceOperationCompleted, userState);
        }

        [SoapDocumentMethod("http://tempuri.org/SetBadImage", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public void SetBadImage(string username, string password, int imageid)
        {
            base.Invoke("SetBadImage", new object[] { username, password, imageid });
        }

        public void SetBadImageAsync(string username, string password, int imageid)
        {
            this.SetBadImageAsync(username, password, imageid, null);
        }

        public void SetBadImageAsync(string username, string password, int imageid, object userState)
        {
            if (this.SetBadImageOperationCompleted == null)
            {
                this.SetBadImageOperationCompleted = new SendOrPostCallback(this.OnSetBadImageOperationCompleted);
            }
            base.InvokeAsync("SetBadImage", new object[] { username, password, imageid }, this.SetBadImageOperationCompleted, userState);
        }

        [SoapDocumentMethod("http://tempuri.org/UploadCaptcha", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public int UploadCaptcha(string username, string password, [XmlElement(DataType="base64Binary")] byte[] pic)
        {
            return (int) base.Invoke("UploadCaptcha", new object[] { username, password, pic })[0];
        }

        public void UploadCaptchaAsync(string username, string password, byte[] pic)
        {
            this.UploadCaptchaAsync(username, password, pic, null);
        }

        public void UploadCaptchaAsync(string username, string password, byte[] pic, object userState)
        {
            if (this.UploadCaptchaOperationCompleted == null)
            {
                this.UploadCaptchaOperationCompleted = new SendOrPostCallback(this.OnUploadCaptchaOperationCompleted);
            }
            base.InvokeAsync("UploadCaptcha", new object[] { username, password, pic }, this.UploadCaptchaOperationCompleted, userState);
        }

        public string Url
        {
            get
            {
                return base.Url;
            }
            set
            {
                if (!((!this.IsLocalFileSystemWebService(base.Url) || this.useDefaultCredentialsSetExplicitly) || this.IsLocalFileSystemWebService(value)))
                {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }

        public bool UseDefaultCredentials
        {
            get
            {
                return base.UseDefaultCredentials;
            }
            set
            {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
    }
}

