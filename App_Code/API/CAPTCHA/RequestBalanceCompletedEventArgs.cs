﻿namespace API.CAPTCHA
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;

    [DesignerCategory("code"), GeneratedCode("System.Web.Services", "4.0.30319.1"), DebuggerStepThrough]
    public class RequestBalanceCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        internal RequestBalanceCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public double Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return (double) this.results[0];
            }
        }
    }
}

