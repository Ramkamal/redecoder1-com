﻿namespace API.CAPTCHA
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;

    [DebuggerStepThrough, GeneratedCode("System.Web.Services", "4.0.30319.1"), DesignerCategory("code")]
    public class UploadCaptchaCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        internal UploadCaptchaCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public int Result
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return (int) this.results[0];
            }
        }
    }
}

