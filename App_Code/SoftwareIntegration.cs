﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

[WebService(Namespace="http://tempuri.org/"), WebServiceBinding(ConformsTo=WsiProfiles.BasicProfile1_1)]
public class SoftwareIntegration : WebService
{
    [WebMethod]
    public DataTable CheckCredential(string username, string password)
    {
        SqlCommand sQL = new SqlCommand("uspCheckUserCredentials_urbb")
        {
            CommandType = CommandType.StoredProcedure
        };
        sQL.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = Convert.ToString(username).Trim();
        sQL.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = Convert.ToString(password).Trim();
        DataTable table = new DataTable();
        table = CommonFunctions.ReturnDatasource(sQL).Tables[0];
        if ((table.Rows.Count > 0) && !(Convert.ToString(table.Rows[0]["UserType"]).ToUpper().Trim() == "CLIENT"))
        {
            return new DataTable();
        }
        return table;
    }
}

