﻿namespace RAHEEL
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Web.Services;
    using System.Web.Services.Description;
    using System.Web.Services.Protocols;

    [WebServiceBinding(Name="RaheelSoap", Namespace="http://tempuri.org/"), DesignerCategory("code"), GeneratedCode("System.Web.Services", "4.0.30319.1"), DebuggerStepThrough]
    public class Raheel : SoapHttpClientProtocol
    {
        private SendOrPostCallback GetBalanceOperationCompleted;
        private SendOrPostCallback getPendingImagesOperationCompleted;
        private SendOrPostCallback getReportsOperationCompleted;
        private SendOrPostCallback GetUserOperationCompleted;
        private SendOrPostCallback SetPaymentOperationCompleted;

        public event GetBalanceCompletedEventHandler GetBalanceCompleted;

        public event getPendingImagesCompletedEventHandler getPendingImagesCompleted;

        public event getReportsCompletedEventHandler getReportsCompleted;

        public event GetUserCompletedEventHandler GetUserCompleted;

        public event SetPaymentCompletedEventHandler SetPaymentCompleted;

        public Raheel()
        {
            string str = ConfigurationManager.AppSettings["RAHEEL.raheel"];
            if (str != null)
            {
                base.Url = str;
            }
            else
            {
                base.Url = "http://www.imagetyperz.com/RAHEEL.ASMX";
            }
        }

        public IAsyncResult BeginGetBalance(int intClientID, AsyncCallback callback, object asyncState)
        {
            return base.BeginInvoke("GetBalance", new object[] { intClientID }, callback, asyncState);
        }

        public IAsyncResult BegingetPendingImages(int botID, AsyncCallback callback, object asyncState)
        {
            return base.BeginInvoke("getPendingImages", new object[] { botID }, callback, asyncState);
        }

        public IAsyncResult BegingetReports(int rpt_type, AsyncCallback callback, object asyncState)
        {
            return base.BeginInvoke("getReports", new object[] { rpt_type }, callback, asyncState);
        }

        public IAsyncResult BeginGetUser(int botID, AsyncCallback callback, object asyncState)
        {
            return base.BeginInvoke("GetUser", new object[] { botID }, callback, asyncState);
        }

        public IAsyncResult BeginSetPayment(string Username, float Amount, int Botid, string OrderNo, string RDUsername, AsyncCallback callback, object asyncState)
        {
            return base.BeginInvoke("SetPayment", new object[] { Username, Amount, Botid, OrderNo, RDUsername }, callback, asyncState);
        }

        public void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        public DataTable EndGetBalance(IAsyncResult asyncResult)
        {
            return (DataTable) base.EndInvoke(asyncResult)[0];
        }

        public DataTable EndgetPendingImages(IAsyncResult asyncResult)
        {
            return (DataTable) base.EndInvoke(asyncResult)[0];
        }

        public DataTable EndgetReports(IAsyncResult asyncResult)
        {
            return (DataTable) base.EndInvoke(asyncResult)[0];
        }

        public DataTable EndGetUser(IAsyncResult asyncResult)
        {
            return (DataTable) base.EndInvoke(asyncResult)[0];
        }

        public void EndSetPayment(IAsyncResult asyncResult)
        {
            base.EndInvoke(asyncResult);
        }

        [SoapDocumentMethod("http://tempuri.org/GetBalance", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public DataTable GetBalance(int intClientID)
        {
            return (DataTable) base.Invoke("GetBalance", new object[] { intClientID })[0];
        }

        public void GetBalanceAsync(int intClientID)
        {
            this.GetBalanceAsync(intClientID, null);
        }

        public void GetBalanceAsync(int intClientID, object userState)
        {
            if (this.GetBalanceOperationCompleted == null)
            {
                this.GetBalanceOperationCompleted = new SendOrPostCallback(this.OnGetBalanceOperationCompleted);
            }
            base.InvokeAsync("GetBalance", new object[] { intClientID }, this.GetBalanceOperationCompleted, userState);
        }

        [SoapDocumentMethod("http://tempuri.org/getPendingImages", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public DataTable getPendingImages(int botID)
        {
            return (DataTable) base.Invoke("getPendingImages", new object[] { botID })[0];
        }

        public void getPendingImagesAsync(int botID)
        {
            this.getPendingImagesAsync(botID, null);
        }

        public void getPendingImagesAsync(int botID, object userState)
        {
            if (this.getPendingImagesOperationCompleted == null)
            {
                this.getPendingImagesOperationCompleted = new SendOrPostCallback(this.OngetPendingImagesOperationCompleted);
            }
            base.InvokeAsync("getPendingImages", new object[] { botID }, this.getPendingImagesOperationCompleted, userState);
        }

        [SoapDocumentMethod("http://tempuri.org/getReports", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public DataTable getReports(int rpt_type)
        {
            return (DataTable) base.Invoke("getReports", new object[] { rpt_type })[0];
        }

        public void getReportsAsync(int rpt_type)
        {
            this.getReportsAsync(rpt_type, null);
        }

        public void getReportsAsync(int rpt_type, object userState)
        {
            if (this.getReportsOperationCompleted == null)
            {
                this.getReportsOperationCompleted = new SendOrPostCallback(this.OngetReportsOperationCompleted);
            }
            base.InvokeAsync("getReports", new object[] { rpt_type }, this.getReportsOperationCompleted, userState);
        }

        [SoapDocumentMethod("http://tempuri.org/GetUser", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public DataTable GetUser(int botID)
        {
            return (DataTable) base.Invoke("GetUser", new object[] { botID })[0];
        }

        public void GetUserAsync(int botID)
        {
            this.GetUserAsync(botID, null);
        }

        public void GetUserAsync(int botID, object userState)
        {
            if (this.GetUserOperationCompleted == null)
            {
                this.GetUserOperationCompleted = new SendOrPostCallback(this.OnGetUserOperationCompleted);
            }
            base.InvokeAsync("GetUser", new object[] { botID }, this.GetUserOperationCompleted, userState);
        }

        private void OnGetBalanceOperationCompleted(object arg)
        {
            if (this.GetBalanceCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.GetBalanceCompleted(this, new GetBalanceCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OngetPendingImagesOperationCompleted(object arg)
        {
            if (this.getPendingImagesCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.getPendingImagesCompleted(this, new getPendingImagesCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OngetReportsOperationCompleted(object arg)
        {
            if (this.getReportsCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.getReportsCompleted(this, new getReportsCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OnGetUserOperationCompleted(object arg)
        {
            if (this.GetUserCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.GetUserCompleted(this, new GetUserCompletedEventArgs(args.Results, args.Error, args.Cancelled, args.UserState));
            }
        }

        private void OnSetPaymentOperationCompleted(object arg)
        {
            if (this.SetPaymentCompleted != null)
            {
                InvokeCompletedEventArgs args = (InvokeCompletedEventArgs) arg;
                this.SetPaymentCompleted(this, new AsyncCompletedEventArgs(args.Error, args.Cancelled, args.UserState));
            }
        }

        [SoapDocumentMethod("http://tempuri.org/SetPayment", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
        public void SetPayment(string Username, float Amount, int Botid, string OrderNo, string RDUsername)
        {
            base.Invoke("SetPayment", new object[] { Username, Amount, Botid, OrderNo, RDUsername });
        }

        public void SetPaymentAsync(string Username, float Amount, int Botid, string OrderNo, string RDUsername)
        {
            this.SetPaymentAsync(Username, Amount, Botid, OrderNo, RDUsername, null);
        }

        public void SetPaymentAsync(string Username, float Amount, int Botid, string OrderNo, string RDUsername, object userState)
        {
            if (this.SetPaymentOperationCompleted == null)
            {
                this.SetPaymentOperationCompleted = new SendOrPostCallback(this.OnSetPaymentOperationCompleted);
            }
            base.InvokeAsync("SetPayment", new object[] { Username, Amount, Botid, OrderNo, RDUsername }, this.SetPaymentOperationCompleted, userState);
        }
    }
}

