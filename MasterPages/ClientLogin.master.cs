﻿using MK;
using System;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MasterPages_ClientLogin : MasterPage
{
    //protected ContentPlaceHolder ContentPlaceHolder1;
    //protected HtmlForm form1;
    //protected HtmlHead Head1;
    //protected Image Image1;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int num = 0;
            string str = "";
            num = Convert.ToInt32(Login.GetId());
            str = Convert.ToString(Login.GetUserType()).ToUpper();
            if ((num != 0) && (str != ""))
            {
                string str2 = str;
                if (str2 == null)
                {
                    goto Label_0078;
                }
                if (!(str2 == "CLIENT"))
                {
                    if (str2 == "BOTUSER")
                    {
                        goto Label_0066;
                    }
                    goto Label_0078;
                }
                base.Response.Redirect(@"~\Forms\ClientHome.aspx");
            }
            return;
        Label_0066:
            base.Response.Redirect(@"~\Forms\BotUser.aspx");
            return;
        Label_0078:
            Login.LogOut();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

