﻿using MK;
using System;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MasterPages_EmptyClientBot : MasterPage
{
    //protected ContentPlaceHolder ContentPlaceHolder1;
    //protected HtmlForm form1;
    //protected HtmlHead Head1;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int num = 0;
            string str = "";
            num = Convert.ToInt32(Login.GetId());
            str = Convert.ToString(Login.GetUserType()).ToUpper();
            if ((num == 0) || (str == ""))
            {
                Login.LogOut();
                base.Response.Redirect(@"~\Forms\Login.aspx");
            }
            else
            {
                string str2;
                if (((str2 = str) == null) || ((str2 != "CLIENT") && (str2 != "BOTUSER")))
                {
                    Login.LogOut();
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

