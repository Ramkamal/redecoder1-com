﻿<%@ WebHandler Language="C#" Class="CheckLogin" %>

using System;
using System.Configuration; 
using Microsoft.VisualBasic; 
using System.Data;
using System.Data.SqlClient;
using MK;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public class CheckLogin : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        context.Response.ContentType = "text/plain";
   
        
        if (context.Request.Form["username"] == null && context.Request.QueryString["username"] == null)
        {
            context.Response.Write("ERROR: INVALID_USERNAME");
            return;
        }

        if (context.Request.Form["password"] == null && context.Request.QueryString["password"] == null)
        {
            context.Response.Write("ERROR: INVALID_PASSWORD");
            return;
        }


        string username = "";
        string password = "";

        username = GetValue(context.Request.Form["username"], context.Request.QueryString["username"]).Trim();
        password = GetValue(context.Request.Form["password"], context.Request.QueryString["password"]).Trim();

        SqlCommand sqlCmd = new SqlCommand("uspCheckUserCredentials_urbb");
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = Convert.ToString(username).Trim();
        sqlCmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = Convert.ToString(password).Trim();
        DataTable dt = new DataTable();
        DataSet ds = CommonFunctions.ReturnDatasource(sqlCmd);
        
        if (ds.Tables.Count > 0)
        {
            dt = ds.Tables[0];
        } 
        


        if (dt.Rows.Count > 0)
        {
            if (Convert.ToString(dt.Rows[0]["UserType"]).ToUpper().Trim() == "CLIENT")
            {
            }
            else
            {
                context.Response.Write("ERROR: INVALID_CREDENTIAL");
                return;
            } 
        }

        if (dt.Rows.Count == 0)
        {
            context.Response.Write("ERROR: INVALID_CREDENTIAL");
            return;
        }
        //else
        //{
        //    context.Response.Write(dt.Rows[0]["ITUSERNAME"] + "," + dt.Rows[0]["ITPASSWORD"]);
        //    return;
        //}
        
    }

    public string GetValue(object obj1, object obj2)
    {
        if (obj1 == null)
        {
            return Convert.ToString(obj2);
        }

        if (obj2 == null)
        {
            return Convert.ToString(obj1);
        }

        return "";
    }

    public bool IsReusable
    {
        get {
            return false;
        }
    }

}