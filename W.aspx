﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="W.aspx.cs" Inherits="W" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="refresh" content="2;url=W.aspx">
</head>
<body>
    <form id="form1" runat="server">
    <div width="100%" align="center">
        <asp:GridView ID="gvData" runat="server" BackColor="White" BorderColor="#336666"
            BorderStyle="Double" BorderWidth="3px" CellPadding="4" 
            GridLines="Horizontal" EmptyDataText="No Pending Images Found" 
            onrowdatabound="gvData_RowDataBound">
            <RowStyle BackColor="White" ForeColor="#333333" />
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        
    </div>
    </form>
</body>
</html>
