﻿using MK;
using System;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_Login : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected Label Label1;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator1;
    //protected RequiredFieldValidator RequiredFieldValidator2;
    //protected TextBox txtPassword;
    //protected TextBox txtUsername;

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                this.CheckLogin();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void CheckLogin()
    {
        int id = 0;
        string usertype = "";
        Login.CheckCredential(this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim(), ref id, ref usertype);
        if ((id == 0) && (usertype == ""))
        {
            this.lblErr.Text = "Please enter valid username and password";
        }
        else
        {
            switch (usertype)
            {
                case "CLIENT":
                    base.Response.Redirect("~/Forms/ClientHome.aspx");
                    return;

                case "BOTUSER":
                    base.Response.Redirect("~/Forms/BotUser.aspx");
                    return;

                case "ADMIN":
                    base.Response.Redirect("~/Forms/Admin.aspx");
                    return;
            }
            this.lblErr.Text = "Please enter valid username and password";
        }
    }

    private void ClearAll()
    {
        this.txtUsername.Text = "";
        this.txtPassword.Text = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string str;
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.ClearAll();
                this.Page.Validate();
                int id = 0;
                str = "";
                Login.CheckUserCredentialOnLoad(ref id, ref str);
                if ((id != 0) || (str != ""))
                {
                    string str2 = str;
                    if (str2 == null)
                    {
                        goto Label_00BB;
                    }
                    if (!(str2 == "CLIENT"))
                    {
                        if (str2 == "BOTUSER")
                        {
                            goto Label_0097;
                        }
                        if (str2 == "ADMIN")
                        {
                            goto Label_00A9;
                        }
                        goto Label_00BB;
                    }
                    base.Response.Redirect("~/Forms/ClientHome.aspx");
                }
            }
            return;
        Label_0097:
            base.Response.Redirect("~/Forms/BotUser.aspx");
            return;
        Label_00A9:
            base.Response.Redirect("~/Forms/Admin.aspx");
            return;
        Label_00BB:
            this.lblErr.Text = str;
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

