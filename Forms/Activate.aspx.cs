﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Forms_Activate : Page, IRequiresSessionState
{
    //protected HtmlForm form1;
    //protected Label lblErr;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (base.Request.QueryString["SID"] == null)
            {
                this.lblErr.Text = "Welcome to ReDecoder.com";
            }
            else if (CommonFunctions.ChangeNullToString(base.Request.QueryString["SID"]) == "")
            {
                this.lblErr.Text = "Welcome to ReDecoder.com";
            }
            else
            {
                SqlCommand oledbCmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "uspActivateClient_nnih"
                };
                Guid guid = new Guid(CommonFunctions.ChangeNullToString(base.Request.QueryString["SID"]));
                oledbCmd.Parameters.Add("@DATA", SqlDbType.UniqueIdentifier).Value = guid;
                CommonFunctions.ExecuteNonQuery(oledbCmd);
                this.lblErr.Text = "Account activated, you can now login to the website.";
            }
        }
        catch
        {
            this.lblErr.Text = "Error on application, please try again later.";
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

