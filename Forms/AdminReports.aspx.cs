﻿using Microsoft.VisualBasic;
using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_AdminReports : Page, IRequiresSessionState
{
    //protected GridView gvData;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int num;
            if (base.Request.QueryString["rpt_type"] == null)
            {
                num = 2;
            }
            else if (!Information.IsNumeric(base.Request.QueryString["rpt_type"]))
            {
                num = 2;
            }
            else
            {
                num = Convert.ToInt32(base.Request.QueryString["rpt_type"]);
            }
            DataTable table = new Raheel { Url = ConfigurationManager.AppSettings["RAHEEL.raheel"] }.getReports(num);
            this.gvData.DataSource = table;
            this.gvData.DataBind();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

