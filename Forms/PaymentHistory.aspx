﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.master" AutoEventWireup="true" CodeFile="PaymentHistory.aspx.cs" Inherits="Forms_PaymentHistory" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%" class="boxborder" align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Client Payment History
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
      
        <tr>
            <td>
                Client  
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlClient" runat="server"  Width="134px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlClient"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 100%; height: 300px; overflow: auto">
                    <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Notes" HeaderText="Notes">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
