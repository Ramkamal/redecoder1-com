﻿using System;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_Contactus : Page, IRequiresSessionState
{
    //protected Button btnSendEmail;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator1;
    //protected RequiredFieldValidator RequiredFieldValidator2;
    //protected RequiredFieldValidator RequiredFieldValidator3;
    //protected RequiredFieldValidator RequiredFieldValidator4;
    //protected TextBox txtDetails;
    //protected TextBox txtEmailid;
    //protected TextBox txtName;
    //protected TextBox txtPhoneNo;

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (!this.Page.IsValid)
            {
                this.lblErr.Text = "Please enter all the required details.";
            }
            else
            {
                this.Sendmail();
            }
        }
        catch
        {
            this.lblErr.Text = "Error on application, please try again later.";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErr.Text = "";
    }

    public void Sendmail()
    {
        string body = ((("Name: " + base.Server.HtmlDecode(this.txtName.Text.Trim()) + "<br/>") + "Emalid: " + base.Server.HtmlDecode(this.txtEmailid.Text.Trim()) + "<br/>") + "Phone No: " + base.Server.HtmlDecode(this.txtPhoneNo.Text.Trim()) + "<br/>") + "Enquiry: " + base.Server.HtmlDecode(this.txtDetails.Text.Trim());
        MF.SendMail("redecodr@gmail.com", body, "New Enquiry", true);
        this.txtDetails.Text = "";
        this.txtEmailid.Text = "";
        this.txtName.Text = "";
        this.lblErr.Text = "Your details has been sent. Thank you for your enquiry. We will contact you as soon as possible.";
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

