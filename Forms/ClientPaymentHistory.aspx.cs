﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_ClientPaymentHistory : Page, IRequiresSessionState
{
    //protected GridView gvClientDetails;
    //protected Label lblErr;

    private void LoadPaymentHistory()
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            SqlCommand sQL = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "uspGetPaymentHistoryReport_kicg"
            };
            sQL.Parameters.Add("@ClientId", SqlDbType.Int).Value = Convert.ToInt32(Login.GetId());
            this.gvClientDetails.DataSource = CommonFunctions.ReturnDatasource(sQL);
            this.gvClientDetails.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.LoadPaymentHistory();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

