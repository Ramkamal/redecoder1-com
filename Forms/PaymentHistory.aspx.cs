﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_PaymentHistory : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected DropDownList ddlClient;
    //protected GridView gvClientDetails;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator2;

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadPaymentHistory();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void LoadclientBotDetails()
    {
        SqlCommand sQL = new SqlCommand("uspGetClientDetailsForDropDown_wlkc")
        {
            CommandType = CommandType.StoredProcedure
        };
        sQL.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = 0;
        this.ddlClient.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.ddlClient.DataTextField = "ClientName";
        this.ddlClient.DataValueField = "ClientId";
        this.ddlClient.DataBind();
    }

    private void LoadPaymentHistory()
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            SqlCommand sQL = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "uspGetPaymentHistoryReport_kicg"
            };
            sQL.Parameters.Add("@ClientId", SqlDbType.Int).Value = this.ddlClient.SelectedValue;
            this.gvClientDetails.DataSource = CommonFunctions.ReturnDatasource(sQL);
            this.gvClientDetails.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.LoadclientBotDetails();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

