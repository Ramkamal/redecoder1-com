﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_ForgotPassword : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator2;
    //protected RequiredFieldValidator RequiredFieldValidator3;
    //protected TextBox txtEmailID;
    //protected TextBox txtUsername;

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                SqlCommand oledbCmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "uspGetPassword_rbff"
                };
                oledbCmd.Parameters.Add("@username", SqlDbType.VarChar).Value = this.txtUsername.Text.Trim();
                oledbCmd.Parameters.Add("@emailid", SqlDbType.VarChar).Value = this.txtEmailID.Text.Trim();
                string password = CommonFunctions.ExecuteScalar(oledbCmd);
                if (password == "")
                {
                    this.lblErr.Text = "Username and Emailid does not match, Please enter valid value";
                }
                else
                {
                    this.Sendmail(password);
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErr.Text = "";
        if (!this.Page.IsPostBack)
        {
            this.txtUsername.Text = "";
            this.txtEmailID.Text = "";
        }
    }

    public void Sendmail(string Password)
    {
        try
        {
            string body = ((("Name: " + base.Server.HtmlDecode(this.txtUsername.Text.Trim()) + "<br/>") + "Emalid: " + base.Server.HtmlDecode(this.txtEmailID.Text.Trim()) + "<br/>") + "Your Password: " + base.Server.HtmlDecode(Password) + "<br/>") + "<a href=\"" + base.Server.HtmlDecode("http://ReDecoder.com/Forms/Login.aspx") + "\">Click here to login</a><br/>";
            CommonFunctions.Sendmail("sales@ReDecoder.COM", this.txtEmailID.Text.Trim(), body, "Your password for ReDecoder.com", true);
            this.txtEmailID.Text = "";
            this.txtUsername.Text = "";
            this.lblErr.Text = "Your password has been sent to your emailid.";
        }
        catch
        {
            this.lblErr.Text = "error while sending details";
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

