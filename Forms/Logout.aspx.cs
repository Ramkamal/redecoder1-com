﻿using MK;
using System;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Forms_Logout : Page, IRequiresSessionState
{
    //protected HtmlForm form1;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Login.LogOut();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
        }
        base.Response.Redirect("~/Forms/Login.aspx");
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

