﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="Forms_FAQ" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div align="center" class="wrap">
        <table class="boxborder" style="text-indent: 0px;text-align: left;border:0px;" width="70%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left"> 
                        <br />
                         <h3>
                        <span class="red">Frequently Asked Questions</span>
                    </h3>
                        
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: Can you do programming as per our needs?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    A: Yes, We can do programming also as per your needs.
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: What is ReDecoder.com?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    A: Our site offer solving services. To help our clients
                    we offer full 24/7 support, specific API and instructions.
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: What languages do you support?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    A: Our programmers can provide API’s for almost any web language as like .NET, C\C++,
                    Java , PHP and Perl . In most cases the specific API will be designed for FREE!
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
           
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: Can I use your system from different IPs?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    Yes! You can use our services from as many IPs as you want, with no restriction.
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr> 
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: How do I load money in my account ?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    A: To load your account you simply have to send money using our payment processor
                    then you will get credited in about 5-10 minutes. In case of delay please announce
                    us using the contact form.
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr> 
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">Q: What payment methods do you accept?</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    A: We accept payments through our payment processor but not only. If you prefer
                    to use other methods just let us know by using the contact form
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <strong style="color: #0160A2;">*** For any other details please contact us here: <a
                        href="Contactus.aspx">Contact Us</a></strong>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
