﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.master" AutoEventWireup="true" CodeFile="SetPaymentHistory.aspx.cs" Inherits="Forms_SetPaymentHistory" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%" class="boxborder" align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Set Payment
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Client
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlClient" runat="server" Width="209px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlClient"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Payment Mode
            </td>
            <td align="left">
                <asp:TextBox ID="txtPaymentMode" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPaymentMode"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Amount
            </td>
            <td align="left">
                <asp:TextBox ID="txtAmount" runat="server" MaxLength="7" Widt h="102px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAmount"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAmount"
                    ErrorMessage="Enter valid amount" MaximumValue="999" MinimumValue="-999" Type="Double"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>
                Notes
            </td>
            <td align="left">
                <asp:TextBox ID="txtNotes" runat="server" MaxLength="500" TextMode="MultiLine" Width="244px"
                    Height="53px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" /><asp:Button
                    ID="btnClear" CssClass="button" runat="server" Text="Clear" CausesValidation="False"
                    OnClick="btnClear_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 100%; height: 300px; overflow: auto">
                    <asp:GridView ID="gvPaymentHistory" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Payment Detail Found</b></div>"
                        DataKeyNames="PaymentHistoryId, ClientID, Notes" Width="100%" OnRowCommand="gvPaymentHistory_RowCommand"
                        OnRowDataBound="gvPaymentHistory_RowDataBound" OnRowDeleting="gvPaymentHistory_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="Username" HeaderText="Client Username">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ClientName" HeaderText="Client Name">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:CommandField HeaderText="Edit" SelectText="Edit" ShowSelectButton="True">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:CommandField>
                            <asp:CommandField HeaderText="Delete" SelectText="Delete" ShowDeleteButton="True">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:CommandField>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
