﻿using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_SetPaymentHistory : Page, IRequiresSessionState
{
    //protected Button btnClear;
    //protected Button btnSubmit;
    //protected DropDownList ddlClient;
    //protected GridView gvPaymentHistory;
    //protected Label lblErr;
    //protected RangeValidator RangeValidator1;
    //protected RequiredFieldValidator RequiredFieldValidator1;
    //protected RequiredFieldValidator RequiredFieldValidator3;
    //protected RequiredFieldValidator RequiredFieldValidator4;
    //protected TextBox txtAmount;
    //protected TextBox txtNotes;
    //protected TextBox txtPaymentMode;

    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearAll();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                SqlCommand oledbCmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure
                };
                if (Convert.ToInt32(this.ViewState["PaymentHistoryId"]) == 0)
                {
                    oledbCmd.CommandText = "uspInsertPayment_exdu";
                }
                else
                {
                    oledbCmd.CommandText = "uspUpdatePayment_fdxy";
                    oledbCmd.Parameters.Add("@PaymentHistoryId", SqlDbType.VarChar, 100).Value = Convert.ToInt32(this.ViewState["PaymentHistoryId"]);
                }
                oledbCmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = this.ddlClient.SelectedValue;
                oledbCmd.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DateTime.Now;
                oledbCmd.Parameters.Add("@PaymentMode", SqlDbType.VarChar, 100).Value = this.txtPaymentMode.Text.Trim();
                oledbCmd.Parameters.Add("@Amount", SqlDbType.Money).Value = this.txtAmount.Text.Trim();
                oledbCmd.Parameters.Add("@Notes", SqlDbType.VarChar, 250).Value = this.txtNotes.Text.Trim();
                if (CommonFunctions.ExecuteNonQuery(oledbCmd))
                {
                    if (Convert.ToInt32(this.ViewState["PaymentHistoryId"]) == 0)
                    {
                        oledbCmd = new SqlCommand
                        {
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "getClientITDetails_hnaz"
                        };
                        oledbCmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = this.ddlClient.SelectedValue;
                        Raheel raheel = new Raheel
                        {
                            Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]
                        };
                        string username = Convert.ToString(CommonFunctions.ReturnDatasource(oledbCmd).Tables[0].Rows[0]["ITUsername"]);
                        raheel.SetPayment(username, Convert.ToSingle(this.txtAmount.Text.Trim()), 0x5aa, "", this.ddlClient.SelectedItem.Text);
                        raheel.Dispose();
                    }
                    this.ClearAll();
                    this.lblErr.Text = "Successfully Saved";
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
            this.lblErr.Text = this.ddlClient.SelectedValue;
        }
    }

    private void ClearAll()
    {
        SqlCommand sQL = new SqlCommand("uspGetPaymentHistory_ecds")
        {
            CommandType = CommandType.StoredProcedure
        };
        this.gvPaymentHistory.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.gvPaymentHistory.DataBind();
        sQL.CommandText = "uspGetClientDetailsForDropDown_wlkc";
        sQL.CommandType = CommandType.StoredProcedure;
        sQL.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = 0;
        this.ddlClient.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.ddlClient.DataValueField = "ClientId";
        this.ddlClient.DataTextField = "ClientName";
        this.ddlClient.DataBind();
        this.txtAmount.Text = "";
        this.txtNotes.Text = "";
        this.txtPaymentMode.Text = "";
        this.ViewState["PaymentHistoryId"] = "0";
    }

    protected void gvPaymentHistory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToUpper() == "SELECT")
            {
                this.ddlClient.SelectedItem.Selected = false;
                this.ddlClient.Items.FindByValue(Convert.ToString(this.gvPaymentHistory.DataKeys[Convert.ToInt32(e.CommandArgument)].Values[1])).Selected = true;
                this.txtNotes.Text = Convert.ToString(this.gvPaymentHistory.DataKeys[Convert.ToInt32(e.CommandArgument)].Values[2]);
                this.txtPaymentMode.Text = base.Server.HtmlDecode(Convert.ToString(this.gvPaymentHistory.Rows[Convert.ToInt32(e.CommandArgument)].Cells[3].Text));
                this.txtAmount.Text = base.Server.HtmlDecode(Convert.ToString(this.gvPaymentHistory.Rows[Convert.ToInt32(e.CommandArgument)].Cells[4].Text));
                this.ViewState["PaymentHistoryId"] = Convert.ToString(this.gvPaymentHistory.DataKeys[Convert.ToInt32(e.CommandArgument)].Values[0]);
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void gvPaymentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton button = (LinkButton)e.Row.Cells[6].Controls[0];
                button.Attributes.Add("onClick", "return confirm('Are you sure to delete?');");
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void gvPaymentHistory_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlCommand oledbCmd = new SqlCommand("uspDeletePaymentHistory_kftr");
            oledbCmd.Parameters.Add("@PaymentHistoryId", SqlDbType.Int).Value = Convert.ToString(this.gvPaymentHistory.DataKeys[e.RowIndex].Values[0]);
            oledbCmd.CommandType = CommandType.StoredProcedure;
            CommonFunctions.ExecuteNonQuery(oledbCmd);
            this.ClearAll();
            this.lblErr.Text = "Successfully Deleted";
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.ClearAll();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

