﻿using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_ClientBalanceReport : Page, IRequiresSessionState
{
    //protected GridView gvClientDetails;
    //protected Label lblErr;

    private void LoadBalanceReport()
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            SqlCommand sQL = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure
            };
            sQL = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "getClientITDetails_hnaz"
            };
            sQL.Parameters.Add("@ClientID", SqlDbType.Int).Value = Convert.ToInt32(Login.GetId());
            int intClientID = Convert.ToInt32(CommonFunctions.ReturnDatasource(sQL).Tables[0].Rows[0]["ITClientID"]);
            Raheel raheel = new Raheel
            {
                Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]
            };
            DataTable balance = raheel.GetBalance(intClientID);
            if (balance.Rows.Count > 0)
            {
                balance.Rows[0]["BalanceImages"] = Convert.ToDecimal(balance.Rows[0]["BalanceAmount"]) / Convert.ToDecimal((double)0.002);
            }
            this.gvClientDetails.DataSource = balance;
            this.gvClientDetails.DataBind();
            raheel.Dispose();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.LoadBalanceReport();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

