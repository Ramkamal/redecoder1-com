﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.master" AutoEventWireup="true" CodeFile="BalanceReport.aspx.cs" Inherits="Forms_BalanceReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%" class="boxborder" align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Balance Report
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td>
                Client 
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlClient" runat="server" Width="134px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="ddlClient" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" 
                    onclick="btnSubmit_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 100%; height: 300px; overflow: auto">
                    <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ClientType" HeaderText="Client Type">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ClientName" HeaderText="ClientName">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NoOfCaptcha" HeaderText="No.of Solving">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TotalImages" HeaderText="Total Solvings">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TimedOutImages" HeaderText="Timed Out Solvings">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessedImages" HeaderText="Processed Solvings">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BadImages" HeaderText="Bad Solvings">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BalanceImages" HeaderText="Balance Solvings">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField> 
                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PaidAmount" HeaderText="Paid Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BalanceAmount" HeaderText="Balance Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
