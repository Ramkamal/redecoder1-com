﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="aLogin.aspx.cs" Inherits="Forms_aLogin" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	
    <div class="wrap" style="display:none"> 
        <table width="100%" >
            <tr style="width: 100%">
                <td valign="top" align="center">
                    <table style="width: 300px;border:0px;" class="boxborder" align="center" cellpadding="5" cellspacing="5">
                        <tr>
                            <td colspan="2" class="tdMainHeader">
                             <h3><span class="red">Login</span></h3>
                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblErr" CssClass="Errormsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="Label1" CssClass="Errormsg" Text="Welcome to ReDecoder.com" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Username
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUsername" runat="server" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" CssClass="Errormsg" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" CssClass="Errormsg" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left"><a target="_self" href="ResendActivationEmail.aspx">Resend Activation Email</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
