﻿using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_Registration : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected CompareValidator CompareValidator1;
    //protected DropDownList ddlCountry;
    //protected Label Label1;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator10;
    //protected RequiredFieldValidator RequiredFieldValidator11;
    //protected RequiredFieldValidator RequiredFieldValidator13;
    //protected RequiredFieldValidator RequiredFieldValidator2;
    //protected RequiredFieldValidator RequiredFieldValidator3;
    //protected RequiredFieldValidator RequiredFieldValidator4;
    //protected RequiredFieldValidator RequiredFieldValidator5;
    //protected RequiredFieldValidator RequiredFieldValidator6;
    //protected RequiredFieldValidator RequiredFieldValidator7;
    //protected RequiredFieldValidator RequiredFieldValidator8;
    //protected RequiredFieldValidator RequiredFieldValidator9;
    //protected TextBox txtClientAddress;
    //protected TextBox txtClientName;
    //protected TextBox txtContactPerson;
    //protected TextBox txtEmailid;
    //protected TextBox txtFAX;
    //protected TextBox txtPassword;
    //protected TextBox txtPhone;
    //protected TextBox txtRetypePassword;
    //protected TextBox txtSecretAnswer;
    //protected TextBox txtSecretQuestion;
    //protected TextBox txtState;
    //protected TextBox txtUsername;

    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearAll();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                SqlCommand oledbCmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure
                };
                if (Login.IsUsernameExists(this.txtUsername.Text.Trim(), Convert.ToInt32(this.ViewState["ClientId"]), 0, 0))
                {
                    this.lblErr.Text = "Username already exists, Please choose another";
                }
                else if (Login.IsEmailExists(this.txtEmailid.Text.Trim(), Convert.ToInt32(this.ViewState["ClientId"])))
                {
                    this.lblErr.Text = "Email id already exists, Please choose another";
                }
                else
                {
                    Raheel raheel = new Raheel
                    {
                        Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]
                    };
                    DataTable user = raheel.GetUser(0x5aa);
                    oledbCmd.CommandText = "uspRegisterClientBotUser_mitb";
                    oledbCmd.Parameters.Add("@SubscriptionId", SqlDbType.Int).Value = 1;
                    oledbCmd.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = this.txtUsername.Text.Trim();
                    oledbCmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = this.txtPassword.Text.Trim();
                    oledbCmd.Parameters.Add("@ClientName", SqlDbType.VarChar, 100).Value = this.txtClientName.Text.Trim();
                    oledbCmd.Parameters.Add("@ClientAddress", SqlDbType.VarChar, 0x9f6).Value = this.txtClientAddress.Text.Trim();
                    oledbCmd.Parameters.Add("@SecretQuestion", SqlDbType.VarChar, 150).Value = this.txtSecretQuestion.Text.Trim();
                    oledbCmd.Parameters.Add("@SecretAnswer", SqlDbType.VarChar, 150).Value = this.txtSecretAnswer.Text.Trim();
                    oledbCmd.Parameters.Add("@State", SqlDbType.VarChar, 100).Value = this.txtState.Text.Trim();
                    oledbCmd.Parameters.Add("@Country", SqlDbType.VarChar, 100).Value = this.ddlCountry.Text.Trim();
                    oledbCmd.Parameters.Add("@ContactPersonName", SqlDbType.VarChar, 100).Value = this.txtContactPerson.Text.Trim();
                    oledbCmd.Parameters.Add("@Phone", SqlDbType.VarChar, 50).Value = this.txtPhone.Text.Trim();
                    oledbCmd.Parameters.Add("@Fax", SqlDbType.VarChar, 50).Value = this.txtFAX.Text.Trim();
                    oledbCmd.Parameters.Add("@Emailid", SqlDbType.VarChar, 150).Value = this.txtEmailid.Text.Trim();
                    oledbCmd.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = false;
                    oledbCmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = false;
                    oledbCmd.Parameters.Add("@Affiliate", SqlDbType.VarChar).Value = CommonFunctions.ChangeNullToString(this.Session["Affiliate"]);
                    oledbCmd.Parameters.Add("@ITClientID", SqlDbType.Int).Value = user.Rows[0]["ClientID"];
                    oledbCmd.Parameters.Add("@ITUsername", SqlDbType.VarChar).Value = user.Rows[0]["username"];
                    oledbCmd.Parameters.Add("@ITPassword", SqlDbType.VarChar).Value = user.Rows[0]["password"];
                    raheel.Dispose();
                    string strValue = CommonFunctions.ExecuteScalar(oledbCmd);
                    if (CommonFunctions.ChangeNullToString(strValue) == "")
                    {
                        this.lblErr.Text = "There is a problem in creating account, Please try again later.";
                    }
                    else
                    {
                        MF.SendConfirmationEmail(this.txtEmailid.Text.Trim(), strValue);
                        this.ClearAll();
                        this.lblErr.Text = "Successfully Created the user. Sent confirmation email, Please check your email account..";
                    }
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void ClearAll()
    {
        this.ddlCountry.SelectedIndex = 0;
        this.txtClientAddress.Text = "";
        this.txtClientName.Text = "";
        this.txtContactPerson.Text = "";
        this.txtEmailid.Text = "";
        this.txtFAX.Text = "";
        this.txtPassword.Text = "";
        this.txtPhone.Text = "";
        this.txtSecretAnswer.Text = "";
        this.txtSecretQuestion.Text = "";
        this.txtState.Text = "";
        this.txtUsername.Text = "";
        this.ViewState["ClientId"] = "0";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                if (base.Request.QueryString["Affiliate"] != null)
                {
                    this.Session["Affiliate"] = base.Request.QueryString["Affiliate"];
                }
                this.ClearAll();
                this.Page.Validate();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

