﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientBot.master" AutoEventWireup="true" CodeFile="ClientPaymentHistory.aspx.cs" Inherits="Forms_ClientPaymentHistory" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 700px;border:0px;" class="boxborder" align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Payment History
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 700px; height: 300px; overflow: auto">
                    <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                        Width="700px">
                        <Columns>
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Amount" HeaderText="Amount">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                           <%-- <asp:BoundField DataField="Notes" HeaderText="Notes">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>--%>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

