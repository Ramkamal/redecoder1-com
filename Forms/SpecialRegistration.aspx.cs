﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_SpecialRegistration : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected CompareValidator CompareValidator1;
    //protected DropDownList ddlClient;
    //protected DropDownList ddlCountry;
    //protected DropDownList ddlSubscription;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator1;
    //protected RequiredFieldValidator RequiredFieldValidator10;
    //protected RequiredFieldValidator RequiredFieldValidator11;
    //protected RequiredFieldValidator RequiredFieldValidator13;
    //protected RequiredFieldValidator RequiredFieldValidator14;
    //protected RequiredFieldValidator RequiredFieldValidator2;
    //protected RequiredFieldValidator RequiredFieldValidator3;
    //protected RequiredFieldValidator RequiredFieldValidator4;
    //protected RequiredFieldValidator RequiredFieldValidator5;
    //protected RequiredFieldValidator RequiredFieldValidator6;
    //protected RequiredFieldValidator RequiredFieldValidator9;
    //protected TextBox txtClientName;
    //protected TextBox txtEmailid;
    //protected TextBox txtPassword;
    //protected TextBox txtRetypePassword;
    //protected TextBox txtSecretAnswer;
    //protected TextBox txtSecretQuestion;
    //protected TextBox txtState;
    //protected TextBox txtUsername;

    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearAll();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                SqlCommand oledbCmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure
                };
                if (Login.IsUsernameExists(this.txtUsername.Text.Trim(), Convert.ToInt32(this.ViewState["ClientId"]), 0, 0))
                {
                    this.lblErr.Text = "Username already exists, Please choose another";
                }
                else if (Login.IsEmailExists(this.txtEmailid.Text.Trim(), Convert.ToInt32(this.ViewState["ClientId"])))
                {
                    this.lblErr.Text = "Email id already exists, Please choose another";
                }
                else
                {
                    oledbCmd.CommandText = "uspRegisterClientBotUser_mitb";
                    oledbCmd.Parameters.Add("@SubscriptionId", SqlDbType.Int).Value = this.ddlSubscription.SelectedValue;
                    oledbCmd.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = this.txtUsername.Text.Trim();
                    oledbCmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = this.txtPassword.Text.Trim();
                    oledbCmd.Parameters.Add("@ClientName", SqlDbType.VarChar, 100).Value = this.txtClientName.Text.Trim();
                    oledbCmd.Parameters.Add("@ClientAddress", SqlDbType.VarChar, 0x9f6).Value = "";
                    oledbCmd.Parameters.Add("@SecretQuestion", SqlDbType.VarChar, 150).Value = this.txtSecretQuestion.Text.Trim();
                    oledbCmd.Parameters.Add("@SecretAnswer", SqlDbType.VarChar, 150).Value = this.txtSecretAnswer.Text.Trim();
                    oledbCmd.Parameters.Add("@State", SqlDbType.VarChar, 100).Value = this.txtState.Text.Trim();
                    oledbCmd.Parameters.Add("@Country", SqlDbType.VarChar, 100).Value = this.ddlCountry.Text.Trim();
                    oledbCmd.Parameters.Add("@ContactPersonName", SqlDbType.VarChar, 100).Value = this.txtClientName.Text.Trim();
                    oledbCmd.Parameters.Add("@Phone", SqlDbType.VarChar, 50).Value = "";
                    oledbCmd.Parameters.Add("@Fax", SqlDbType.VarChar, 50).Value = "";
                    oledbCmd.Parameters.Add("@Emailid", SqlDbType.VarChar, 150).Value = this.txtEmailid.Text.Trim();
                    oledbCmd.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = false;
                    oledbCmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = true;
                    oledbCmd.Parameters.Add("@ClientID", SqlDbType.Int).Value = this.ddlClient.SelectedValue;
                    if (CommonFunctions.ExecuteNonQuery(oledbCmd))
                    {
                        this.ClearAll();
                        this.lblErr.Text = "Successfully Created the user, Please log in to use the website.";
                    }
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void ClearAll()
    {
        SqlCommand sQL = new SqlCommand("uspGetLatestSubscriptionDetails_vbni")
        {
            CommandType = CommandType.StoredProcedure
        };
        this.ddlSubscription.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.ddlSubscription.DataTextField = "SubscriptionName";
        this.ddlSubscription.DataValueField = "SubscriptionId";
        this.ddlSubscription.DataBind();
        this.txtClientName.Text = "";
        this.txtEmailid.Text = "";
        this.txtPassword.Text = "";
        this.txtSecretAnswer.Text = "";
        this.txtSecretQuestion.Text = "";
        this.txtState.Text = "";
        this.txtUsername.Text = "";
        this.LoadclientBotDetails();
        this.ViewState["ClientId"] = "0";
    }

    private void LoadclientBotDetails()
    {
        SqlCommand sQL = new SqlCommand("uspGetClientDetailsForDropDown_wlkc")
        {
            CommandType = CommandType.StoredProcedure
        };
        sQL.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = 1;
        this.ddlClient.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.ddlClient.DataTextField = "ClientName";
        this.ddlClient.DataValueField = "ClientId";
        this.ddlClient.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.ClearAll();
                this.Page.Validate();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

