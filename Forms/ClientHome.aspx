﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientBot.master" AutoEventWireup="true" CodeFile="ClientHome.aspx.cs" Inherits="Forms_ClientHome" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .headers
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            color: Gray;
        }
    </style>
    <table>
        <tr>
            <td align="left">
                <table style="border: 0px;" class="boxborder" width="80%" align="center" cellpadding="5"
                    cellspacing="5">
                    <tr>
                        <td colspan="2" align="left">
                            <h1>
                                Welcome to ReDecoder.com</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            ReDecoder is offering the fastest and the quickest solving service around. We are
                            focused on providing an out of the box service to get the quickest response time
                            you can imagine. If you are still looking to get quick response time for the Solvings,
                            you have come to the right place!
                        </td>
                    </tr>

                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                     
                    

                    <tr>
                        <td colspan="2" align="left">
                            <ul>
                                <li>We are charging 0.005$ per Solving</li>
                                <li>You pay only for the right Solving</li>
                                <li>We have our own API to work with your softwares.</li>
                                <li>We have 24/7 online support</li>
                                <li>Above 95% accuracy and best response time guaranteed with no charge for bad Solvings.</li></ul>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="tdMainHeader" align="left" style="text-align: left;">
                            Balance Report
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width: 700px; overflow: auto">
                                <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                                    ShowHeader="false" Width="700px">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <br />
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="font-size: x-large">
                                                                <span class="headers">Balance Solvings:</span>
                                                                <%# DataBinder.Eval(Container.DataItem, "BalanceImages")%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="headers">Total Solvings:</span>
                                                            <%# DataBinder.Eval(Container.DataItem, "TotalImages")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="headers">Timed Out Solvings:</span>
                                                            <%# DataBinder.Eval(Container.DataItem, "TimedOutImages")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="headers">Processed Solvings:</span>
                                                            <%# DataBinder.Eval(Container.DataItem, "ProcessedImages")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <span class="headers">Bad Solvings:</span>
                                                            <%# DataBinder.Eval(Container.DataItem, "BadImages")%>
                                                        </td>
                                                    </tr> 
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Left" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="AlternateStyle" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td align="left">
                <table style="background-color: rgb(233,233,233); border: 0px;" width="80%" class="boxborder"
                    align="center" cellpadding="5" cellspacing="5"> 
                     <tr>
                        <td colspan="2" class="tdMainHeader" align="left">
                            <h3 style="text-align: left;">Low Balance Alert</h3>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2" align="left">
                            You will recieve an email alert once your credit drops below the 500 solvings. We will send notification on the email you have in your account.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>


        <tr>
            <td align="left">
                <table style="background-color: rgb(233,233,233); border: 0px;" width="80%" class="boxborder"
                    align="center" cellpadding="5" cellspacing="5">
                    <tr>
                        <td colspan="2" class="tdMainHeader" align="left">
                            <h3 style="text-align: left;">
                                Order Solvings</h3>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <span class="tdMainHeader">Order using Avangate payment gateway</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            Supports all major Credit/Debit Cards, PayPal and WebMoney
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <table cellpadding="0" cellspacing="0" style="width: 70%;">
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="tdMainHeader" style="font-size: 10pt;">5K Solvings</span>
                                    </td>
                                    <td align="right" width="30%">
                                        Only US$6 per package&nbsp;(30$ Total)
                                    </td>
                                    <td align="center" width="10%">
                                        <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                            color: White;">
                                            <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=5&CART=2">
                                                Order Now </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="tdMainHeader" style="font-size: 10pt;">25K Solvings</span>
                                    </td>
                                    <td align="right" width="30%">
                                        Only US$5 per package&nbsp;(125$ Total)
                                    </td>
                                    <td align="center" width="10%">
                                        <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                            color: White;">
                                            <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=25&CART=2">
                                                Order Now </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <span class="tdMainHeader" style="font-size: 10pt;">100K Solvings</span>
                                    </td>
                                    <td align="right" width="40%">
                                        Only US$4.5 per package&nbsp;(450$ Total)
                                    </td>
                                    <td align="center" width="10%">
                                        <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                            color: White;">
                                            <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=100&CART=2">
                                                Order Now </a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
