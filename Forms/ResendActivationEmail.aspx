﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="ResendActivationEmail.aspx.cs" Inherits="Forms_ResendActivationEmail" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div align="center">
        <table class="boxborder" style="border:0px;" cellpadding="2" cellspacing="2" style="text-align:left">
            <tr>
                <td align="left" colspan="4">
                    <br />
                    <h3>
                        <span class="red">Resend Activation Email</span>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    &nbsp; &nbsp;
                    <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Username
                </td>
                <td align="left">
                    <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" CssClass="Errormsg" ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUsername"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Emailid
                </td>
                <td align="left">
                    <asp:TextBox ID="txtEmailID" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" CssClass="Errormsg" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailID"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button" OnClick="btnSubmit_Click" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
