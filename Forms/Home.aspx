﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Forms_Home" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
<div id="bar">
        <div class="wrap">
            <span class="step"><a>1</a> Register</span> <span class="step"><a>2</a> Load Credit</span>
            <span class="step"><a>3</a> Send Solving</span>
        </div>
    </div>
    <div class="wrap">
        <table>
            <tr>
                <td>
                    ReDecoder is offering the fastest and the quickest solving service
                    around. We are focused on providing an out of the box service to get the quickest
                    response time you can imagine. If you are still looking to get quick response time
                    for the Solvings, you have come to the right place!
                </td>
            </tr>
            <tr>
            <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <ul>
                        <li>We are charging 0.005$ per Solving</li>
                        <li>You pay only for the right Solving</li>
                        <li>We have our own API to work with your softwares.</li>
                        <li>We have 24/7 online support</li>
                        <li>Above 95% accuracy and best response time guaranteed with no charge for bad Solvings.</li></ul>
                </td>
            </tr>
            <tr>
            <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <div class="col">
                        <h3>
                            24/7/365<span class="red"> uninterrupted service</span></h3>
                        <p>
                            Our service has run for years (from April 2008) with only several time down. It
                            is hard to keep this service stable for long time. But fortunately we have several
                            very good programmers working on this service. Our programmers not only work for
                            this service, but also work for our other softwares. They are the secret of our
                            very stable service.
                        </p>
                    </div>
                    <div class="col">
                        <h3>
                            Professional <span class="red">technical support</span></h3>
                        <p>
                            We have a group of very good programmers. Our expertise includes LAMP system, PHP/Python/Perl/Rudy/JavaScript/C/C++/C#
                            programming and network solution. We can help you on integrating our service into
                            your software for free. We can even give you some help on your own field if you
                            need.
                        </p>
                    </div>
                    <div class="col last">
                        <h3>
                            No <span class="red">hidden cost</span></h3>
                        <p>
                            With our online tools, you can get all details on your usage. You can easily know
                            how and when each credit is used. There is no cheating, no hidden fee. Everything
                            is transparent. If you are not satisfied on what we have offered online, feel free
                            to tell us what you want more, we will make it for you.
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="col">
                        <h3>
                            Use our service to <span class="red">support life of our workers</span></h3>
                        <p>
                            We hire workers to work on our project not only to make money for ourselves, but
                            also to make our workers live better with much better salary than other local workers
                            without any special skills.
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
