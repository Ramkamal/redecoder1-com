﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="PrivacyPolicy.aspx.cs" Inherits="Forms_PrivacyPolicy" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="width: 100%;text-align:center;" >
        <table class="boxborder" style="text-indent:0px;"  width="80%" width="100%" cellpadding="0"
            cellspacing="0">
            <tr>
                <td align="left">
                    <br />
                    <br />
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 13pt;">
                        PRIVACY POLICY</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 851px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Account</p>
                    <p style="text-align: left;">
                        ReDecoder.com will not make public your email address or any sensitive information
                        from your account. You are allowed to use our services from unlimited number of
                        IPs.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 851px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Password recovery</p>
                    <p style="text-align: left;">
                        To recover your password just use the password recovery option from the menu. The
                        password will be sent to the email id you registered .</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 851px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Refund Policy</p>
                    <p style="text-align: left;">
                        ReDecoder.com accept refund policy in case the promised service is not well provided,
                        the serviced stopped or not working as expected. The client has the right to request
                        a refund anytime by negotiating with our representants. Any request will be made
                        by the form in the contact page.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
