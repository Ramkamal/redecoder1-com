﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="Affiliates.aspx.cs" Inherits="Forms_Affiliates" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="width: 100%; text-align: center;">
        <table class="boxborder" style="text-indent: 0px;" width="80%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 13pt;">
                        <br />
                        <br />
                        Affiliates</p>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <p>
                        <strong>Promote our software on your website and get paid for it!</strong>
                        <br />
                        Get <b>10.00</b>% commission on every sale you refer! <strong>Become a ReDecoder affiliate
                            today!</strong> &nbsp;
                    </p>
                    <p>
                        Our software products are available for sale through the <a href="http://www.avangate.com/affiliates/"
                            target="_blank" title="http://www.avangate.com/affiliates/">Avangate Affiliate Network</a>.
                    </p>
                    <p>
                        To start selling, all you need to do is display product information and &ldquo;Buy
                        Now&rdquo; links on your website.
                    </p>
                    <p>
                        <strong>Guaranteed Affiliate Commission</strong>
                        <br />
                        When a visitor follows a link or banner from your site, Avangate <em>keeps track of
                            your Affiliate ID for 120 calendar days</em>. That's 4 months during which any
                        order placed by the same user guarantees your affiliate commission.
                    </p>
                    <p>
                        <strong>It's as easy as 1, 2, 3...</strong>
                        <br />
                        Avangate takes care of the ordering process, product delivery and customer support.
                        <br />
                        - No need to store any software on your servers, or keep track of licenses
                        <br />
                        - No hassles related to invoicing or payments from customers
                        <br />
                        - No setup fee, no start up cost, no hidden performance targets to reach!
                        <br />
                        You will also benefit from comprehensive affiliate reports that will show you exactly
                        how you perform.
                    </p>
                    <p>
                        <strong>Monthly payments</strong>
                        <br />
                        You will get your commissions every month, either transferred to your Avangate Prepaid
                        Debit MasterCard or sent via wire transfer, check or PayPal.
                    </p>
                    <p>
                        <strong>Start selling now!</strong>
                        <br />
                        <em>Use every available channel of promotion you have - website, newsletter, etc!</em>
                        Remember - the more referral links you create, the more chances you have to make
                        money from your web site!
                    </p>
                    <p>
                        <a href="https://www.avangate.com/affiliates/sign-up.php?merchant=IMAGETYP&amp;template=Default+Template&amp;lang=en">
                            Join our Affiliate Program!</a>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
