﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientBot.master" AutoEventWireup="true" CodeFile="RechargeAccount.aspx.cs" Inherits="Forms_RechargeAccount" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="background-color: rgb(233,233,233); width: 700px; border: 0px;" class="boxborder"
        align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader" align="left">
                <h3 style="text-align: left;">
                    Order Solvings</h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <span class="tdMainHeader">Order using Avangate payment gateway</span>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                Supports all major Credit/Debit Cards, PayPal and WebMoney
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table cellpadding="0" cellspacing="0" style="width: 80%;">
                    <tr>
                        <td align="left" width="30%">
                            <span class="tdMainHeader" style="font-size: 10pt;">5K Solvings</span>
                        </td>
                        <td align="right" width="30%">
                            Only US$6 per package&nbsp;(30$ Total)
                        </td>
                        <td align="center" width="20%">
                            <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                color: White;">
                                <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=5&CART=2">
                                    Order Now </a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <span class="tdMainHeader" style="font-size: 10pt;">25K Solvings</span>
                        </td>
                        <td align="right" width="30%">
                            Only US$5 per package&nbsp;(125$ Total)
                        </td>
                        <td align="center" width="10%">
                            <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                color: White;">
                                <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=25&CART=2">
                                    Order Now </a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="30%">
                            <span class="tdMainHeader" style="font-size: 10pt;">100K Solvings</span>
                        </td>
                        <td align="right" width="40%">
                            Only US$4.5 per package&nbsp;(450$ Total)
                        </td>
                        <td align="center" width="10%">
                            <div style="width: 77px; height: 25px; background: url('../Images/btnBack.png');
                                color: White;">
                                <a target="_blank" style="text-align: center; color: White;" href="https://secure.avangate.com/order/checkout.php?PRODS=4551764&QTY=100&CART=2">
                                    Order Now </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
