﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientBot.master" AutoEventWireup="true" CodeFile="ClientBalanceReport.aspx.cs" Inherits="Forms_ClientBalanceReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .headers
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            color: Gray;
        }
    </style>
    <table style="width: 700px;border:0px;" class="boxborder"  align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Balance Report
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 700px;  overflow: auto">
                    <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                        Width="700px" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                  
                                    <br />
                                    <table width="100%">
                                         <%--<tr>
                                            <td align="left">
                                                <div style="font-size:x-large"><span class="headers" style="font-size:x-large">Balance Amount in $:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "BalanceAmount")%></div>
                                            </td>
                                        </tr> --%>
                                        <tr>
                                                        <td align="left">
                                                         <div style="font-size: x-large">
                                                            <span class="headers">Balance Solvings:</span>
                                                            <%# DataBinder.Eval(Container.DataItem, "BalanceImages")%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                        <tr>
                                            <td align="left">
                                                <span class="headers">Total Solvings:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "TotalImages")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <span class="headers">Timed Out Solvings:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "TimedOutImages")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <span class="headers">Processed Solvings:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "ProcessedImages")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <span class="headers">Bad Solvings:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "BadImages")%>
                                            </td>
                                        </tr> 
                                       <%-- <tr>
                                            <td align="left">
                                                <span class="headers">Total Amount in $:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "TotalAmount")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <span class="headers">Total amount paid so far in $:</span>
                                                <%# DataBinder.Eval(Container.DataItem, "PaidAmount")%>
                                            </td>
                                        </tr> --%>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Left" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
