﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_ChangeProfile : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected DropDownList ddlCountry;
    //protected Label lblErr;
    //protected Label lblUsername;
    //protected RequiredFieldValidator RequiredFieldValidator10;
    //protected RequiredFieldValidator RequiredFieldValidator11;
    //protected RequiredFieldValidator RequiredFieldValidator4;
    //protected RequiredFieldValidator RequiredFieldValidator5;
    //protected RequiredFieldValidator RequiredFieldValidator6;
    //protected RequiredFieldValidator RequiredFieldValidator7;
    //protected RequiredFieldValidator RequiredFieldValidator9;
    //protected TextBox txtClientAddress;
    //protected TextBox txtClientName;
    //protected TextBox txtContactPerson;
    //protected TextBox txtEmailid;
    //protected TextBox txtFAX;
    //protected TextBox txtPassword;
    //protected TextBox txtPhone;
    //protected TextBox txtRetypePassword;
    //protected TextBox txtSecretAnswer;
    //protected TextBox txtSecretQuestion;
    //protected TextBox txtState;

    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.ClearAll();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                if (this.txtPassword.Text.Trim() != this.txtRetypePassword.Text.Trim())
                {
                    this.lblErr.Text = "New and Retype password must be same";
                }
                else
                {
                    SqlCommand oledbCmd = new SqlCommand
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    if (Login.IsEmailExists(this.txtEmailid.Text.Trim(), Convert.ToInt32(this.ViewState["ClientId"])))
                    {
                        this.lblErr.Text = "Email id already exists, Please choose another";
                    }
                    else
                    {
                        oledbCmd.CommandText = "uspUpdateClientBotUser_jutw";
                        oledbCmd.Parameters.Add("@ClientId", SqlDbType.Int).Value = Convert.ToInt32(this.ViewState["ClientId"]);
                        oledbCmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = this.txtPassword.Text.Trim();
                        oledbCmd.Parameters.Add("@ClientName", SqlDbType.VarChar, 100).Value = this.txtClientName.Text.Trim();
                        oledbCmd.Parameters.Add("@ClientAddress", SqlDbType.VarChar, 0x9f6).Value = this.txtClientAddress.Text.Trim();
                        oledbCmd.Parameters.Add("@SecretQuestion", SqlDbType.VarChar, 150).Value = this.txtSecretQuestion.Text.Trim();
                        oledbCmd.Parameters.Add("@SecretAnswer", SqlDbType.VarChar, 150).Value = this.txtSecretAnswer.Text.Trim();
                        oledbCmd.Parameters.Add("@State", SqlDbType.VarChar, 100).Value = this.txtState.Text.Trim();
                        oledbCmd.Parameters.Add("@Country", SqlDbType.VarChar, 100).Value = this.ddlCountry.Text.Trim();
                        oledbCmd.Parameters.Add("@ContactPersonName", SqlDbType.VarChar, 100).Value = this.txtContactPerson.Text.Trim();
                        oledbCmd.Parameters.Add("@Phone", SqlDbType.VarChar, 50).Value = this.txtPhone.Text.Trim();
                        oledbCmd.Parameters.Add("@Fax", SqlDbType.VarChar, 50).Value = this.txtFAX.Text.Trim();
                        oledbCmd.Parameters.Add("@Emailid", SqlDbType.VarChar, 150).Value = this.txtEmailid.Text.Trim();
                        if (CommonFunctions.ExecuteNonQuery(oledbCmd))
                        {
                            this.ClearAll();
                            this.lblErr.Text = "Successfully Updated.";
                        }
                    }
                }
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void ClearAll()
    {
        this.ddlCountry.SelectedIndex = 0;
        this.txtClientAddress.Text = "";
        this.txtClientName.Text = "";
        this.txtContactPerson.Text = "";
        this.txtEmailid.Text = "";
        this.txtFAX.Text = "";
        this.txtPassword.Text = "";
        this.txtPhone.Text = "";
        this.txtSecretAnswer.Text = "";
        this.txtSecretQuestion.Text = "";
        this.txtState.Text = "";
        this.lblUsername.Text = Login.GetUsername();
        this.ViewState["ClientId"] = Login.GetId();
        this.LoadDetails();
    }

    private void LoadDetails()
    {
        try
        {
            SqlCommand sQL = new SqlCommand("uspGetUserDetailsByID_bqyx")
            {
                CommandType = CommandType.StoredProcedure
            };
            sQL.Parameters.Add("@ClientID", SqlDbType.Int).Value = Login.GetId();
            DataTable table = CommonFunctions.ReturnDatasource(sQL).Tables[0];
            this.lblUsername.Text = Convert.ToString(table.Rows[0]["username"]);
            this.txtSecretQuestion.Text = Convert.ToString(table.Rows[0]["SecretQuestion"]);
            this.txtSecretAnswer.Text = Convert.ToString(table.Rows[0]["SecretAnswer"]);
            this.txtClientName.Text = Convert.ToString(table.Rows[0]["ClientName"]);
            this.txtContactPerson.Text = Convert.ToString(table.Rows[0]["ContactPersonName"]);
            this.txtClientAddress.Text = Convert.ToString(table.Rows[0]["ClientAddress"]);
            this.txtState.Text = Convert.ToString(table.Rows[0]["State"]);
            this.ddlCountry.SelectedItem.Selected = false;
            this.ddlCountry.Items.FindByText(Convert.ToString(table.Rows[0]["Country"])).Selected = true;
            this.txtEmailid.Text = Convert.ToString(table.Rows[0]["Emailid"]);
            this.txtPhone.Text = Convert.ToString(table.Rows[0]["Phone"]);
            this.txtFAX.Text = Convert.ToString(table.Rows[0]["Fax"]);
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.ClearAll();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

