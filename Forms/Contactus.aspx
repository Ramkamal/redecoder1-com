﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="Contactus.aspx.cs" Inherits="Forms_Contactus" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div align="center">
        <table style="text-align: left;border:0;" cellpadding="2" cellspacing="2"  class="boxborder">
            <tr>
                <td colspan="2">
                    <br />
                    <h3>
                        <span class="red">Contact us </span>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                Your Name
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtName" runat="server" MaxLength="100" Width="196px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailid" runat="server" MaxLength="100" Width="196px" TabIndex="1"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmailid"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone No
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtPhoneNo" runat="server" MaxLength="100" Width="196px" TabIndex="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhoneNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Enter your Enquiry details here
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDetails" runat="server" Height="119px" MaxLength="250" TextMode="MultiLine"
                                    Width="202px" TabIndex="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDetails"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSendEmail" runat="server" OnClick="btnSendEmail_Click" Text="Send"
                                    TabIndex="4" CssClass="button" Width="114px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="left">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px">
                                &nbsp;
                            </td>
                            <td align="left" style="height: 27px">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <a href="mailto:redecodr@gmail.com">redecodr@gmail.com</a>
                            </td>
                        </tr> 
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
