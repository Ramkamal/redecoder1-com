﻿using MK;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_ClientDetailsReport : Page, IRequiresSessionState
{
    //protected GridView gvClientDetails;
    //protected Label lblErr;

    private void LoadclientBotDetails()
    {
        SqlCommand sQL = new SqlCommand("uspGetClientList_hydc")
        {
            CommandType = CommandType.StoredProcedure
        };
        this.gvClientDetails.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.gvClientDetails.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.LoadclientBotDetails();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

