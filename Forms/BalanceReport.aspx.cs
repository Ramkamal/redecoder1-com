﻿using MK;
using RAHEEL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forms_BalanceReport : Page, IRequiresSessionState
{
    //protected Button btnSubmit;
    //protected DropDownList ddlClient;
    //protected GridView gvClientDetails;
    //protected Label lblErr;
    //protected RequiredFieldValidator RequiredFieldValidator2;

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadBalanceReport();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.LoadclientBotDetails();
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    private void LoadBalanceReport()
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            Raheel raheel = new Raheel
            {
                Url = ConfigurationManager.AppSettings["RAHEEL.raheel"]
            };
            DataTable balance = raheel.GetBalance(Convert.ToInt32(this.ddlClient.SelectedValue));
            if (balance.Rows.Count > 0)
            {
                balance.Rows[0]["BalanceImages"] = Convert.ToDecimal(balance.Rows[0]["BalanceAmount"]) / Convert.ToDecimal((double)0.002);
            }
            this.gvClientDetails.DataSource = balance;
            this.gvClientDetails.DataBind();
            raheel.Dispose();
        }
    }

    private void LoadclientBotDetails()
    {
        SqlCommand sQL = new SqlCommand("uspGetClientDetailsForDropDown_wlkc")
        {
            CommandType = CommandType.StoredProcedure
        };
        sQL.Parameters.Add("@IsBotUser", SqlDbType.Bit).Value = 0;
        this.ddlClient.DataSource = CommonFunctions.ReturnDatasource(sQL);
        this.ddlClient.DataTextField = "ClientName";
        this.ddlClient.DataValueField = "ITClientID";
        this.ddlClient.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblErr.Text = "";
            if (!this.Page.IsPostBack)
            {
                this.LoadclientBotDetails();
            }
        }
        catch (Exception exception)
        {
            CommonFunctions.WriteError(exception);
            this.lblErr.Text = CommonVariables.SystemErrorMsg;
        }
    }

    //protected HttpApplication ApplicationInstance
    //{
    //    get
    //    {
    //        return this.Context.ApplicationInstance;
    //    }
    //}

    //protected DefaultProfile Profile
    //{
    //    get
    //    {
    //        return (DefaultProfile)this.Context.Profile;
    //    }
    //}
}

