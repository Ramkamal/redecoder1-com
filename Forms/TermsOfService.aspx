﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ClientLogin.master" AutoEventWireup="true" CodeFile="TermsOfService.aspx.cs" Inherits="Forms_TermsOfService" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="width: 100%;text-align:center;">
        <table class="boxborder" style="text-indent:0px;"  width="80%" cellpadding="0"
            cellspacing="0">
            <tr>
                <td align="left">
                    <br />
                    <br />
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 13pt;">
                        TERMS OF SERVICE</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 801px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Legal</p>
                    <p style="text-align: left;">
                        ReDecoder.com is not responsible for the applications, scripts and any other utilities
                        that use our service. Any product using this service cannot violate Digital Millennium
                        Copyright Act or any other international law. We reserve the right to stop providing
                        the service in case the client use it for suspicious or illegal activities.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 801px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Hacks and malware</p>
                    <p style="text-align: left;">
                        ReDecoder.com don’t allow the use of our services for illegal software or activities
                        like any kind of spam, viruses, trojans, malware or any other malicious software
                        that can cause our services to be banned/backlisted.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 801px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Blocking account</p>
                    <p style="text-align: left;">
                        In case we detect any suspicious activity we keep the right to block the account
                        for an unlimited period. In this case the client will be notices by email. The client
                        have the right to know the reasons and to negotiate the reactivation of his account.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 801px;">
                    <p class="MsoNormal" style="color: green; text-align: left; font-size: 11pt;">
                        Source code</p>
                    <p style="text-align: left;">
                        You have the right to use the provided source code as you like except you can’t
                        use it with another service. By using our services you declare that your
                        product/software doesn’t violate any law.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
