﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.master" AutoEventWireup="true" CodeFile="ClientDetailsReport.aspx.cs" Inherits="Forms_ClientDetailsReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%" width="100%" class="boxborder" align="center" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2" class="tdMainHeader">
                Client Details Report
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblErr" runat="server" CssClass="Errormsg"></asp:Label>
            </td>
        </tr>
        
         <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" >
                <div style="width: 100%; height: 300px; overflow: auto">
                    <asp:GridView ID="gvClientDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="<div style='width:100%;text-align:center;'><b>No Details Found</b></div>"
                        Width="100%">
                        <Columns>
                            
                            <asp:BoundField DataField="UserType" HeaderText="User Type">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ClientName" HeaderText="ClientName">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ContactPersonName" HeaderText="Contact Person Name">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Username" HeaderText="Username">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Password" HeaderText="Password">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Emailid" HeaderText="Email id">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="State" HeaderText="State">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Country" HeaderText="Country">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField> 
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="AlternateStyle" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
